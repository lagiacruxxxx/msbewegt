-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2016 at 02:38 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `msbewegt`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogger`
--

DROP TABLE IF EXISTS `blogger`;
CREATE TABLE IF NOT EXISTS `blogger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `img_active` varchar(255) NOT NULL,
  `img_inactive` varchar(255) NOT NULL,
  `entry_header` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `blogger`
--

INSERT INTO `blogger` (`id`, `name`, `desc`, `img_active`, `img_inactive`, `entry_header`, `ordering`) VALUES
(1, 'Martin', 'Martin G. lebt seit 16 Jahren mit der Diagnose MS. Er hat einen 26 jährigen Sohn, ein Haus gebaut undeinen Baum in seinem Garten gepflanzt. MS kann ihn nicht aufhalten, denn MS ist behandelbar.', 'martin_active.png', 'martin_inactive.png', 'blogheader_martin.png', 1),
(2, 'Christine', 'Christine H. lebt seit 5 Jahren mit der Diagnose MS. Christine radelt von Traunstein nach Salzburg in eineinhalb Stunden. MS bremst sie nicht aus, denn MS ist behandelbar.', 'christine_active.png', 'christine_inactive.png', 'blogheader_christine.png', 2),
(3, 'Julia', 'Julia Z. lebt seit 4 Jahren mit der Diagnose MS. Vor kurzem startete sie eine Ausbildung zur Schmuckdesignerin. MS hat ihren Lebensmut nicht gebrochen, denn MS ist behandelbar. ', '1457357753_H7swWzr3bkh.png', '1457357756_7V5N8Oz4IsA.png', '1457357761_fBHaXc0ccRUX.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `blog_entry`
--

DROP TABLE IF EXISTS `blog_entry`;
CREATE TABLE IF NOT EXISTS `blog_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `blogger_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `teaser_text` text NOT NULL,
  `prettyurl` varchar(255) NOT NULL,
  `detail_img` varchar(255) DEFAULT NULL,
  `header` text CHARACTER SET latin1,
  `detail_type` int(11) DEFAULT NULL,
  `detail_html` text CHARACTER SET latin1,
  `detail_html_mobile` text CHARACTER SET latin1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `blog_entry`
--

INSERT INTO `blog_entry` (`id`, `name`, `blogger_id`, `created_date`, `teaser_text`, `prettyurl`, `detail_img`, `header`, `detail_type`, `detail_html`, `detail_html_mobile`) VALUES
(7, 'Neues Jahr - neues Glück', 1, '2016-03-04', 'Ich kann mich nur wiederholen: Wenn ich persönlich nicht genügend Bewegung mache, meldet sich sofort meine MS. Im letzten Jahr kam ich aufgrund meiner privaten Umstände nicht zum Laufen - im Grunde wurde die MS von Tag zu Tag schlechter. Dies hatte ich schon im Vorfeld befürchtet ...', 'neues-jahr-neues-glueck', 'image_upload_placeholder.png', 'Neues Jahr - neues Glück', 0, '', ''),
(8, 'Valencia', 1, '2016-02-19', 'Weiß nicht ob unter Euch ein paar Fußball-Freunde sind. Wenn ja, dann wisst ihr ja, dass nicht erst seit gestern die drittgrößte Stadt Spaniens kein gutes Pflaster für den österreichischen Fußball darstellt ;). Da lief es für mich ...', 'valencia', 'image_upload_placeholder.png', 'Valencia', 0, '', ''),
(9, 'Elastizitätsprobleme', 2, '2016-02-26', 'Manchmal spüre ich meine MS (und ein bisschen auch mein Alter) ein wenig stärker, gerade auch im Alltag. Altersbeschwerden plus Zusatz quasi. Aber von ein paar abgebröselten Knochenverbindungsteilen, Bänder-, Sehnen- und Muskel-Überdehnungen ...', 'elastizitaetsprobleme', 'image_upload_placeholder.png', 'Elastizitätsprobleme', 0, '', ''),
(10, 'Der Tag der Liebenden', 2, '2016-02-12', 'Auch wenn ich nicht zu den größten Anhängerinnen des Valentinstages zähle (ich hab kein Blumengeschäft ;) ), so kann man im Leben trotzdem nicht genügend Zuneigung und Liebe geben und entgegen nehmen. Somit ein schönes Wochenende und einen schönen Valentinstag!', 'der-tag-der-liebenden', 'image_upload_placeholder.png', 'Der Tag der Liebenden', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('8bdd323c2a343e38d8128077a4282c3f29dc902f', '::1', 1457077602, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037373630323b757365725f69647c733a313a2231223b),
('b4b38b3b3d028393757b7553427d67a806f23ee9', '::1', 1457077301, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037373330313b757365725f69647c733a313a2231223b),
('a48a4f2bcbadf7b79b7792308c726eb9f78e1b68', '::1', 1457076930, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037363933303b757365725f69647c733a313a2231223b),
('eb59c1b8f11b6b13b33a1a20396e15dd9f1413cf', '::1', 1457076460, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037363436303b757365725f69647c733a313a2231223b),
('1a01abe3b7c021a8cc3b3d42421c68d0c589dd87', '::1', 1457076157, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037363135373b757365725f69647c733a313a2231223b),
('6c352b7d738b9aa08953cf9b35bee57b4909340d', '::1', 1457075726, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037353732363b757365725f69647c733a313a2231223b),
('52de34170e11091f4f3ca0545244a6f9562454bf', '::1', 1457075390, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037353339303b757365725f69647c733a313a2231223b),
('3c846c1f8561cafc8c3e55a73ca7a1eb265fef92', '::1', 1457074938, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037343933383b757365725f69647c733a313a2231223b),
('d4cae1cc23e56d6b446df1aa035908b0668d2851', '::1', 1457074595, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037343539353b757365725f69647c733a313a2231223b),
('b4bf139a46775e186e98b01afe312c30e1a715a3', '::1', 1457073929, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037333932393b757365725f69647c733a313a2231223b),
('d265605216f1d380ec8247033adaec5ebce6dde5', '::1', 1457071564, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037313536343b757365725f69647c733a313a2231223b),
('ae9fa2988432872b0a58bf99f7622f3e4911a69b', '::1', 1457071263, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037313236333b757365725f69647c733a313a2231223b),
('f9f02b699bf5190c64202bf099828c73e8a529ed', '::1', 1457070838, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037303833383b757365725f69647c733a313a2231223b),
('510e87a0104a5297a0e971f0a3dc70701c7354c2', '::1', 1457070454, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037303435343b757365725f69647c733a313a2231223b),
('33e790f0e4106dd01c4c32fbeee9034237ae7d81', '::1', 1457070117, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037303131373b757365725f69647c733a313a2231223b),
('61ae7729e907e74b5dd7e92abac47f46f89b6e73', '::1', 1457069812, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036393831323b757365725f69647c733a313a2231223b),
('669b7cb357f00893abe5371a5831dea2b1d20d1a', '::1', 1457069373, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036393337333b757365725f69647c733a313a2231223b),
('b897d43052940e55f73e80be6fbffb5d8a7e1658', '::1', 1457069062, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036393036323b757365725f69647c733a313a2231223b),
('2d8f85179e2331583d1c858dd03f21a0f460f0cf', '::1', 1457068685, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036383638353b757365725f69647c733a313a2231223b),
('bf72a68283c053284cc05552a78820bdb8475460', '::1', 1457068382, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036383338323b757365725f69647c733a313a2231223b),
('8e2d48b8ba18aba4ca99b9022e5a7e446c064397', '::1', 1457068058, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036383035383b757365725f69647c733a313a2231223b),
('9eb560f70cd02115a8124d66f35f6b4726194a8e', '::1', 1457067642, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036373634323b757365725f69647c733a313a2231223b),
('d41e396b04327db16e8ca280b9e3f0425bd0ab08', '::1', 1457067313, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036373331333b757365725f69647c733a313a2231223b),
('632ecea90e09ab91066c3c603cb2c4c52583ade8', '::1', 1457067011, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036373031313b757365725f69647c733a313a2231223b),
('5b60d12305dc454e326b4b0149dfa798bc225b2c', '::1', 1457066426, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036363432363b757365725f69647c733a313a2231223b),
('9bed26dbf9230c4b71ca3634c72eddb3cae1c314', '::1', 1457064290, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036343237383b757365725f69647c733a313a2231223b),
('20a6b1333acf11e7bbd5947d6c86bbff9391c34b', '::1', 1457065441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036353434313b757365725f69647c733a313a2231223b),
('fc23dd3aafbb02758e183086194392c189d21969', '::1', 1457065786, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036353738363b757365725f69647c733a313a2231223b),
('f3d3f30f2ab2c23171a363a16d3f954a2b84fe63', '::1', 1457066094, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373036363039343b757365725f69647c733a313a2231223b),
('3a1a7557f59aafa0b4641dd1fa75a9577969bbef', '80.108.21.137', 1457078365, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037383232353b757365725f69647c733a313a2231223b),
('846d5410b7fff18e0d711fa84eebbe5116a47557', '80.110.18.29', 1457079016, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037383734313b757365725f69647c733a313a2231223b),
('3c007ec4b3980e5ed5b37eff6b1b5a56e23f1bfe', '80.110.18.29', 1457079056, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373037393034353b757365725f69647c733a313a2231223b),
('e02c196ea5a2e6c6e33829c3ce54b8a15db619cb', '178.188.103.116', 1457086416, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373038363431363b),
('8a8481d28335b1f2134127eb96d54cae3576599e', '178.188.103.116', 1457086845, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373038363831313b757365725f69647c733a313a2231223b),
('4a8fef055e8b401b7712c57a7110ae0d6c880986', '::1', 1457087994, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373038373731323b757365725f69647c733a313a2231223b),
('694fb0bf2439b4b640d43c2f2e8f13c6d37f6a2d', '::1', 1457088294, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373038383239343b757365725f69647c733a313a2231223b),
('e8627f252f808683458082c96272b5f94c5500ae', '::1', 1457089182, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373038393138323b757365725f69647c733a313a2231223b),
('752855226cedd16280284cdeb4060cfaf2db0801', '::1', 1457090655, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039303630383b757365725f69647c733a313a2231223b),
('36cb5fad591723947e171da77776f14ad27f074d', '::1', 1457091085, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039313032373b757365725f69647c733a313a2231223b),
('4b6da3f3e9be52e5082c2c58bb9f425206fea682', '::1', 1457091950, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039313638383b757365725f69647c733a313a2231223b),
('0147bd40445a33b6d8de442b1e95307dff3d564a', '::1', 1457092496, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039323333383b757365725f69647c733a313a2231223b),
('7659bbe438a5278fcb98fce82922dc863286b68d', '::1', 1457092761, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039323634373b757365725f69647c733a313a2231223b),
('5cd1e6657f8ad607cabb53c4b91c500a03866822', '::1', 1457093563, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039333333383b757365725f69647c733a313a2231223b),
('3f6b5715cd91505270d966c1e899e80031436a77', '::1', 1457094012, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039333732323b757365725f69647c733a313a2231223b),
('539957182a945f5fe96d26112664f1bbbd32dadc', '::1', 1457094179, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039343133303b757365725f69647c733a313a2231223b),
('225b8e5702b5314c026fd489ed963877d7acb289', '::1', 1457094725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039343632333b757365725f69647c733a313a2231223b),
('d377f8e40139e9b7506579c43a3a2209f65888cd', '::1', 1457095351, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039353230393b757365725f69647c733a313a2231223b),
('4b4658b780202c3c648112284e46ec58689a7557', '::1', 1457095607, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039353537343b757365725f69647c733a313a2231223b),
('70066205d643f4740b8bd2dc1cdb3ad1a7457081', '::1', 1457097719, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039373531323b757365725f69647c733a313a2231223b),
('dcbc908c9ec43d268aaa38ebe541833337b3eb7c', '::1', 1457098173, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039373836343b757365725f69647c733a313a2231223b),
('c811cadb8d979bb5c33e288748fb6e61d6624f70', '::1', 1457098506, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039383238333b757365725f69647c733a313a2231223b),
('f68dfa74b7b8bc0f59d49da6eb6b10880e193167', '::1', 1457099034, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039383735303b757365725f69647c733a313a2231223b),
('313f6688d8bd0636f2bbe750173a2d8566168558', '::1', 1457099367, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039393036343b757365725f69647c733a313a2231223b),
('49e0e960aa000b1b31f5027114ca410997760169', '::1', 1457099608, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039393336373b757365725f69647c733a313a2231223b),
('3478ff0b34c8268215bde2e11ea0eccc33b16d71', '::1', 1457099956, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373039393733333b757365725f69647c733a313a2231223b),
('019c1c236f9433f21ed312e56c80441f97dcf8c0', '::1', 1457100655, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373130303536343b757365725f69647c733a313a2231223b),
('80c65b789af2a97656fcad6a7b731a65d58cb5e4', '::1', 1457101296, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373130313031343b757365725f69647c733a313a2231223b),
('206ce49b8ff959ccd0d4f3290d887ea476f7d759', '::1', 1457101602, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373130313334303b757365725f69647c733a313a2231223b),
('2a3fd065cd869c1928f552d701271d9b4ec8abff', '::1', 1457103313, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373130333239393b757365725f69647c733a313a2231223b),
('c5fa9058a653f5281b0184a7a786964c01d7a95d', '::1', 1457346234, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373334363230383b757365725f69647c733a313a2231223b),
('796cdf31d9a011cba5684a763fd052609a1f25b0', '::1', 1457347184, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373334373130353b757365725f69647c733a313a2231223b),
('11cdc28909ad2b1270f24bf702d53db456bf9bb3', '::1', 1457348268, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373334383232323b757365725f69647c733a313a2231223b),
('31bcefdc5a3535d6b3bd65a11303445cea0455bd', '::1', 1457348619, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373334383534323b757365725f69647c733a313a2231223b),
('11edb59123c22b93eb99e69c62e6ec2636d1f94c', '::1', 1457349097, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373334383838333b757365725f69647c733a313a2231223b),
('cd9a741229820e07eaee12cc602a8b183838f393', '::1', 1457349498, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373334393231313b757365725f69647c733a313a2231223b),
('6b3821628a7fc231fbdfbe4cc76cce118738f2cc', '::1', 1457349744, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373334393531333b757365725f69647c733a313a2231223b),
('acf2d9182f767bdb255442f334916575fecfca67', '::1', 1457350525, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335303236353b757365725f69647c733a313a2231223b),
('05542ab74c78d36aca0d90236cb4eaa9cd61da66', '::1', 1457350984, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335303833303b757365725f69647c733a313a2231223b),
('82d7156d4d25bafa7d6f6a418e063191bcf82281', '::1', 1457351429, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335313133333b757365725f69647c733a313a2231223b),
('ecce208c0594110f5a7458e9f001f193eb800bbd', '::1', 1457351740, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335313433363b757365725f69647c733a313a2231223b),
('275021d1367988c8d66a80c0a1e47c2c59a103d2', '::1', 1457352018, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335313734303b757365725f69647c733a313a2231223b),
('bf1fef4372abe52a0d4e607f2f068e02af42c9d4', '::1', 1457352135, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335323034383b757365725f69647c733a313a2231223b),
('8991b527011a4f487cb6a96e6d3154ff352822f1', '::1', 1457352661, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335323632383b757365725f69647c733a313a2231223b),
('f59c4ba7044ceef8c5200c478b034530164e456f', '::1', 1457353125, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335333032383b757365725f69647c733a313a2231223b),
('1c3fb7501766d380a61008f1f04628e63a63d969', '::1', 1457353725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335333339343b757365725f69647c733a313a2231223b),
('92ffc170a390f0925386436771532b4b742dce9b', '::1', 1457353725, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335333732353b757365725f69647c733a313a2231223b),
('f1373ad869bcbaea9811ab78e00b99ae1d8a4372', '::1', 1457354539, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335343234323b757365725f69647c733a313a2231223b),
('7af69bab766045fd279885344605f0771c3c64c0', '::1', 1457354866, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335343536313b757365725f69647c733a313a2231223b),
('25301eb57608a9e011f6d56c153da041e0459a13', '::1', 1457354869, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335343836383b757365725f69647c733a313a2231223b),
('a2b9ab607c193b67dd757af3600996d0a754c9f4', '::1', 1457355641, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335353336343b757365725f69647c733a313a2231223b),
('7a90beced59664fada449a4e958656d770ddbf91', '::1', 1457355965, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335353731313b757365725f69647c733a313a2231223b),
('a55c9135cf29b6e98a8a945fe436a132f2cebee8', '::1', 1457356670, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335363530323b757365725f69647c733a313a2231223b),
('b0b1e9401bf97ca54ff7e5cff9e8740794ac98dd', '::1', 1457357591, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335373330343b757365725f69647c733a313a2231223b),
('06d9e4544a5c029886e11d18c2b0f45c781074ec', '::1', 1457357812, 0x5f5f63695f6c6173745f726567656e65726174657c693a313435373335373734383b757365725f69647c733a313a2231223b);

-- --------------------------------------------------------

--
-- Table structure for table `home_elements`
--

DROP TABLE IF EXISTS `home_elements`;
CREATE TABLE IF NOT EXISTS `home_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '999999',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `detail_img` varchar(255) NOT NULL DEFAULT '',
  `detail_html` text NOT NULL,
  `detail_html_mobile` varchar(2000) NOT NULL DEFAULT '',
  `detail_type` int(11) NOT NULL DEFAULT '0',
  `header` varchar(2000) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '1',
  `image` varchar(255) NOT NULL DEFAULT '1.png',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `show` int(11) NOT NULL DEFAULT '1',
  `prettyurl` varchar(255) NOT NULL DEFAULT '',
  `lang` int(11) NOT NULL DEFAULT '0',
  `teaser` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `name`, `detail_img`, `detail_html`, `detail_html_mobile`, `detail_type`, `header`, `type`, `image`, `created_date`, `show`, `prettyurl`, `lang`, `teaser`) VALUES
(1, 'Leben und Arbeiten mit MS', '1457074598_hHn8RLgvag3k.png', '', '', 0, 'Leben und Arbei<dfn class="dictionary-of-numbers">ten mit MS</dfn>', 1, '1.png', '2016-03-03 12:53:33', 1, 'leben-und-arbeiten-mit-ms', 0, 0),
(2, 'BotschafterInnen', '1457077130_ttgtxSGKbN.png', '', '', 0, 'BotschafterInnen', 1, '1.png', '2016-03-04 07:38:37', 1, 'botschafterinnen', 0, 0),
(3, 'Über "Multiple Sklerose"', '1457077545_oT2KvXUaxMiW.png', '', '', 0, 'Über "Multiple Sklerose"', 1, '1.png', '2016-03-04 07:45:41', 1, 'ueber-multiple-sklerose', 0, 0),
(4, 'Leben und Arbeiten mit MS (CLONE)', '1457074598_hHn8RLgvag3k.png', '', '', 0, 'Leben und Arbei<dfn class="dictionary-of-numbers">ten mit MS</dfn>', 1, '1.png', '2016-03-03 12:53:33', 1, 'leben-und-arbeiten-mit-ms', 0, 0),
(5, 'Kontakt', 'image_upload_placeholder.png', '', '', 0, 'Kontakt', 1, '1.png', '2016-03-07 11:55:31', 1, 'kontakt', 0, 0),
(6, 'Impressum', 'image_upload_placeholder.png', '', '', 0, 'Impressum', 1, '1.png', '2016-03-07 11:55:40', 1, 'impressum', 0, 0),
(7, 'Datenschutz', 'image_upload_placeholder.png', '', '', 0, 'Datenschutz', 1, '1.png', '2016-03-07 11:55:51', 1, 'datenschutz', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `item_galleryitem`
--

DROP TABLE IF EXISTS `item_galleryitem`;
CREATE TABLE IF NOT EXISTS `item_galleryitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `credits` varchar(2000) NOT NULL DEFAULT '',
  `item_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menuitem`
--

DROP TABLE IF EXISTS `menuitem`;
CREATE TABLE IF NOT EXISTS `menuitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item_id` int(11) NOT NULL,
  `ordering` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `menuitem`
--

INSERT INTO `menuitem` (`id`, `name`, `item_id`, `ordering`) VALUES
(1, 'NEWS', -2, '0'),
(2, 'ÜBER DIE STIFTUNG', 1, '1'),
(3, 'ÜBER DIE INITIATIVE ''MS BEWEGT''', 1, '2'),
(4, 'ÜBER MULTIPLE SKLEROSE', 3, '3'),
(5, '365 TAGE MIT MS', -1, '4'),
(6, 'DIE EXPERTENSTIMMEN', 2, '5'),
(7, 'ARBEITEN MIT MS', 1, '6'),
(8, 'KONTAKT', 5, ''),
(9, 'IMPRESSUM', 6, ''),
(10, 'DATENSCHUTZ', 7, '');

-- --------------------------------------------------------

--
-- Table structure for table `metatag_category`
--

DROP TABLE IF EXISTS `metatag_category`;
CREATE TABLE IF NOT EXISTS `metatag_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `metatag_item`
--

DROP TABLE IF EXISTS `metatag_item`;
CREATE TABLE IF NOT EXISTS `metatag_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `metatag_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `metatag_id` (`metatag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `module_download`
--

DROP TABLE IF EXISTS `module_download`;
CREATE TABLE IF NOT EXISTS `module_download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `top` int(11) NOT NULL,
  `description` varchar(1000) NOT NULL DEFAULT '',
  `item_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `module_html`
--

DROP TABLE IF EXISTS `module_html`;
CREATE TABLE IF NOT EXISTS `module_html` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `item_type` int(11) NOT NULL DEFAULT '0',
  `html` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `module_image`
--

DROP TABLE IF EXISTS `module_image`;
CREATE TABLE IF NOT EXISTS `module_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `top` int(11) NOT NULL,
  `item_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `module_image`
--

INSERT INTO `module_image` (`id`, `item_id`, `column_id`, `fname`, `top`, `item_type`) VALUES
(7, 1, 1, '1457075178_XUD5epzyoqfu.png', 320, 0),
(2, 4, 1, '1457075178_XUD5epzyoqfu.png', 320, 0),
(16, 1, 1, '1457354345_7rY0i89SGIrU.jpg', 0, 1),
(9, 8, 1, '1457098429_tKRi7omXaJCx.jpg', 0, 1),
(14, 9, 1, '1457101154_mvCGfOWGbzTH.jpg', 0, 1),
(17, 1, 1, '1457354561_ko6uTZRdfC8L.jpg', 0, 2),
(21, 2, 1, '1457354840_ccFzbJ3DStTC.jpg', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `module_text`
--

DROP TABLE IF EXISTS `module_text`;
CREATE TABLE IF NOT EXISTS `module_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `content` text NOT NULL,
  `item_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `module_text`
--

INSERT INTO `module_text` (`id`, `item_id`, `column_id`, `top`, `content`, `item_type`) VALUES
(33, 1, 1, 440, '<span style="color:#60c0d0">Fragen zu Arbeitsrecht und Behindertengleichstellung? Informationen zum barrierefreien Arbeiten? Das Sozial-ministerium Service hilft weiter.</span><br><a target="_blank" href="http://www.orf.at/"><span style="color:#60c0d0">MEHR ERFAHREN</span></a>', 0),
(32, 1, 1, 170, '<span style="color:#60c0d0"><strong><u>BARBARA L.</u></strong>&nbsp;lebt seit<dfn class="dictionary-of-numbers">&nbsp;25 Jahren mit derDiagnose </dfn>MS, sie arbeitet als Kunsterzieherin. MS hindert sie nicht daran ihre SchülerInnen zu inspirieren, denn MS ist behandelbar.</span>', 0),
(31, 1, 0, 0, '<span style="color:#3e3d40">Nahezu jede Österreicherin, jeder Österreicher hatte schon einmal Kontakt mit MS-Betroffenen. Dennoch sind die Informationen über den Verlauf, Therapien und Prognosen ungenau oder oft sogar falsch. Hinzu kommen zahlreiche Vorurteile, die vor allem die Arbeit betreffen - etwa, dass ein regelmäßiges Berufsleben mit MS nicht möglich sei oder die Erkrank<dfn class="dictionary-of-numbers">ten zwangsl</dfn>äufig einen Rollstuhl benötigen.<br>Dank moderner Therapieformen und Medikamente ist das Verbleiben im Berufsleben heute schon in sehr vielen Fällen möglich. Mit einigen zusätzlichen Tipps und Tricks für ArbeitnehmerInnen und ArbeitgeberInnen kann der Arbeitsalltag noch besser bewältigt werden.</span><br><br><span style="color:#60c0d0">ARBEITNEHMER/INNEN ― Informationen und Tipps für MS PatientInnen im Arbeits- und Berufsleben</span><br><a target="_blank" href="http://www.orf.at/"><span style="color:#60c0d0">MEHR ERFAHREN</span></a><br><br><span style="color:#60c0d0">ARBEITGEBER/INNEN ― Informationen und Tipps für ArbeitgeberInnen die MS PatientInnen beschäftigen</span><br><a href="http://www.orf.at"><span style="color:#60c0d0">MEHR ERFAHREN</span></a>', 0),
(28, 2, 0, 0, '<span style="color:#3e3d40"><u><strong>Barbara L.</strong></u><br>lebt seit<dfn class="dictionary-of-numbers">&nbsp;25 Jahren mit der&nbsp;</dfn>Diagnose MS, sie arbeitet als Kunsterzieherin. MS hindert sie nicht daran ihre SchülerInnen zu inspirieren, denn MS ist behandelbar.</span>', 0),
(27, 2, 0, 510, '<span style="color:#3e3d40"><u><strong>Martin G.</strong></u><br>lebt seit<dfn class="dictionary-of-numbers">&nbsp;16 Jahren mit der&nbsp;</dfn>Diagnose MS, er hat einen<dfn class="dictionary-of-numbers">&nbsp;26 j</dfn>ährigen Sohn, ein Haus gebaut und einen Baum im Gar<dfn class="dictionary-of-numbers">ten selbst gepflanzt</dfn>.MS kann ihn nicht aufhalten, denn MS ist behandelbar.</span>', 0),
(26, 2, 0, 1020, '<span style="color:#3e3d40"><u><strong>Christine H.</strong></u><br>lebt seit<dfn class="dictionary-of-numbers">&nbsp;5 Jahren mit der&nbsp;</dfn>Diagnose MS. Christine radelt von Traunstein nach Salzburg in eineinhalb Stunden. MS bremst sie nicht aus, denn MS ist behandelbar.</span>', 0),
(15, 3, 0, 0, '<span style="color:#3e3d40"><strong>Was ist Multiple Sklerose?</strong><br>MS ist die häufigste neurologische Erkrankung des jungen Erwachsenenalters, die zu relevanten Behinderungen führen kann. Nach einer Erhebung der&nbsp;</span><a href="http://www.msgoe.co.at/" onclick="return !window.open(this.href)"><span style="color:#3e3d40">Österreichischen Multiple Sklerose Gesellschaft (ÖMSG)</span></a><span style="color:#3e3d40">&nbsp;aus dem Jahr&nbsp;2011 leben inÖsterreich&nbsp;12.500 Menschen mit der&nbsp;Diagnose MS. Der Erkrankungsbeginn liegt dabei überwiegend zwischen dem 20. und 40. Lebensjahr. MS ist eine&nbsp;<strong>entzündliche Erkrankung des zentralen Nervensystems</strong>&nbsp;und begleitet die Betroffenen während des gesamten Lebens.<br>In ca. 90% aller Fälle beginnt die Erkrankung mit einem Erkrankungsschub. Darunter versteht man relativ akut auftretende neurologische Symptome, die sich wieder gut rückbilden, oder aber auch zu einer bleibenden Verschlechterung führen können. Daraus leitet sich die Bedeutung einer schubvorbeugenden Behandlung ab. Bei 10% der MS-Betroffenen verläuft die Erkrankung ohne Schübe und langsam fortschreitend.<br><br><strong>Ist Multiple Sklerose behandelbar?</strong><br>Ein frühzeitiger Therapiebeginn erhöht die Chance, den Krankheitsverlauf positiv zu beeinflussen und zielt darauf ab, eine Balance im Immunsystem herzustellen. Man spricht von einer&nbsp;<strong>schubvorbeugenden Basistherapie</strong>. Die therapeutischen Fortschritte der letzten Jahre machen eine&nbsp;Verminderung der entzündlichen Krankheitsaktivität möglich, eventuell können Schübe und das Fortschreiten der Multiplen Sklerose&nbsp;soweit gestoppt werden, dass von einem Zustand „frei von Krankheitsaktivität“ gesprochen wird. Auch medikamentöse Behandlungen, die auf einzelne Symptome ausgerichtet sind, und die Neurorehabilitation sind wichtig und aussichtsreich, ganz besonders bei progredient-fortschreitenden Krankheitsverläufen.<br>Erfolgversprechende Therapieoptionen umfassen neben einer&nbsp;<strong>medikamentösen Basistherapie</strong>, die das Immunsystem beeinflusst, auch sinnvoll eingesetzte&nbsp;<strong>komplementäre Behandlungsformen</strong>. Maßnahmen, die eine Stärkung der eigenen Persönlichkeit unterstützen, Entspannungstherapien, Physiotherapie, Hilfe durch soziale Dienste und verschiedene weitere Behandlungsprogramme sind von großer Bedeutung. Innerhalb einer&nbsp;<strong>gemeinsamen, therapeutischen Entscheidungsfindung</strong>&nbsp;von Arzt und Patient wollen MS-Betroffene als gleichberechtigte Partner angesehen werden. Eine komplette Information zu den verfügbaren&nbsp;</span><a href="http://www.msgoe.co.at/index.php?option=com_content&amp;view=article&amp;id=45&amp;Itemid=37" onclick="return !window.open(this.href)"><span style="color:#3e3d40">Therapieoptionen</span></a><span style="color:#3e3d40">&nbsp;ist die Grundvoraussetzung für den Erfolg eines therapeutischen Entscheidungsprozesses.<br><br><strong>Wer erkrankt an Multiple Sklerose?</strong><br>Ca. 70% der Erkrankten sind Frauen. Es wird überwiegend als gesichert angesehen, dass neben einer genetischen Disposition (Empfänglichkeit) auch Umwelteinflüsse, z.B. Kontakte zu Viren, Sonnenbestrahlung, Ernährung und andere Gegebenheiten, sowie der unwägbare Zufall eines Zusammentreffens von Faktoren zu einem bestimmten Zeitpunkt für das Auftreten der Erkrankung eine&nbsp;Rolle spielen. MS ist nicht übertragbar.<br><br><strong>Ist Multiple Sklerose vererbbar?</strong><br>Die Ursache der MS ist nicht ausreichend geklärt. Die Häufigkeit beträgt in Österreich etwa&nbsp;140 pro&nbsp;100.000 Menschen. MS ist keine erbliche Erkrankung. Es müssen mehrere Faktoren zusammenkommen, damit MS entsteht - eine genetische Empfänglichkeit spielt jedoch eine gewisse Rolle. Das Erkrankungsrisiko ist für Verwandte von MS-Patienten nur relativ gering&nbsp;höher als für nicht verwandte Personen.</span>', 0),
(16, 4, 1, 170, '<span style="color:#60c0d0"><strong><u>BARBARA L.</u></strong>&nbsp;lebt seit<dfn class="dictionary-of-numbers">&nbsp;25 Jahren mit derDiagnose </dfn>MS, sie arbeitet als Kunsterzieherin. MS hindert sie nicht daran ihre SchülerInnen zu inspirieren, denn MS ist behandelbar.</span>', 0),
(17, 4, 0, 0, '<span style="color:#3e3d40">Nahezu jede Österreicherin, jeder Österreicher hatte schon einmal Kontakt mit MS-Betroffenen. Dennoch sind die Informationen über den Verlauf, Therapien und Prognosen ungenau oder oft sogar falsch. Hinzu kommen zahlreiche Vorurteile, die vor allem die Arbeit betreffen - etwa, dass ein regelmäßiges Berufsleben mit MS nicht möglich sei oder die Erkrank<dfn class="dictionary-of-numbers">ten zwangsl</dfn>äufig einen Rollstuhl benötigen.<br>Dank moderner Therapieformen und Medikamente ist das Verbleiben im Berufsleben heute schon in sehr vielen Fällen möglich. Mit einigen zusätzlichen Tipps und Tricks für ArbeitnehmerInnen und ArbeitgeberInnen kann der Arbeitsalltag noch besser bewältigt werden.</span><br><br><span style="color:#60c0d0">ARBEITNEHMER/INNEN ― Informationen und Tipps für MS PatientInnen im Arbeits- und Berufsleben</span><br><a target="_blank" href="http://www.orf.at/"><span style="color:#60c0d0">MEHR ERFAHREN</span></a><br><br><span style="color:#60c0d0">ARBEITGEBER/INNEN ― Informationen und Tipps für ArbeitgeberInnen die MS PatientInnen beschäftigen</span><br><a href="http://www.orf.at"><span style="color:#60c0d0">MEHR ERFAHREN</span></a>', 0),
(18, 4, 1, 440, '<span style="color:#60c0d0">Fragen zu Arbeitsrecht und Behindertengleichstellung? Informationen zum barrierefreien Arbeiten? Das Sozial-ministerium Service hilft weiter.</span><br><a target="_blank" href="http://www.orf.at/"><span style="color:#60c0d0">MEHR ERFAHREN</span></a>', 0),
(52, 7, 0, 0, '<strong>Impressum</strong><br><strong>ÖMSG gemeinnützige Privatstiftung (MS-Stiftung Österreich)<br><a href="http://www.ms-stiftung.at/">http://www.ms-stiftung.at/</a></strong>', 0),
(56, 1, 1, 200, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1),
(29, 7, 1, 130, '<strong>Martin G.</strong>&nbsp;lebt seit 16 Jahren mit der Diagnose MS. Er hat einen 26 jährigen Sohn, ein Haus gebaut und einen Baum in seinem Garten gepflanzt. MS kann ihn nicht aufhalten, denn MS ist behandelbar.', 1),
(30, 7, 0, 0, 'Ich kann mich nur wiederholen: Wenn ich persönlich nicht genügend Bewegung mache, meldet sich sofort meine MS. Im letzten Jahr kam ich aufgrund meiner privaten Umstände nicht zum Laufen - im Grunde wurde die MS von Tag zu Tag schlechter.&nbsp;Dies hatte ich schon im Vorfeld befürchtet, aber trotzdem hat es mich ziemlich frustriert. Jetzt beginnt ein neues (Trainings)-Jahr und ich bin motiviert!', 1),
(37, 8, 1, 130, '<strong>Martin G.</strong>&nbsp;lebt seit 16 Jahren mit der Diagnose MS. Er hat einen 26 jährigen Sohn, ein Haus gebaut und einen Baum in seinem Garten gepflanzt. MS kann ihn nicht aufhalten, denn MS ist behandelbar.', 1),
(36, 8, 0, 0, 'Weiß nicht ob unter Euch ein paar Fußball-Freunde sind. Wenn ja, dann wisst ihr ja, dass nicht erst seit gestern die drittgrößte Stadt Spaniens kein gutes Pflaster für den österreichischen Fußball darstellt ;). Da lief es für mich - im wahrsten Sinne des Wortes - letztes Jahr besser. Ohne echte Vorbereitung (die Scheidung hatte mich sämtliche Ressourcen gekostet) bin ich im November dort den Marathon gelaufen und konnte diesen auch beenden. Für mich ein großartiges Glücksgefühl!', 1),
(47, 9, 1, 120, '<strong><u>Christine H.</u></strong> lebt seit 5 Jahren mit der Diagnose MS. Christine radelt von Traunstein nach Salzburg in eineinhalb Stunden. MS bremst sie nicht aus, denn MS ist behandelbar.', 1),
(46, 9, 0, 0, 'Manchmal spüre ich meine MS (und ein bisschen auch mein Alter) ein wenig stärker, gerade auch im Alltag. Altersbeschwerden plus Zusatz quasi. Aber von ein paar abgebröselten Knochenverbindungsteilen, Bänder-, Sehnen- und Muskel-Überdehnungen lasse ich mich so gut es geht nicht unterkriegen!', 1),
(45, 10, 1, 120, '<strong><u>Christine H.</u></strong> lebt seit 5 Jahren mit der Diagnose MS. Christine radelt von Traunstein nach Salzburg in eineinhalb Stunden. MS bremst sie nicht aus, denn MS ist behandelbar.', 1),
(44, 10, 0, 0, 'Auch wenn ich nicht zu den größten Anhängerinnen des Valentinstages zähle (ich hab kein Blumengeschäft ;) ), so kann man im Leben trotzdem nicht genügend Zuneigung und Liebe geben und entgegen nehmen. Somit ein schönes Wochenende und einen schönen Valentinstag!', 1),
(48, 5, 0, 0, '<strong>Impressum</strong><br><strong>ÖMSG gemeinnützige Privatstiftung (MS-Stiftung Österreich)<br><a href="http://www.ms-stiftung.at/">http://www.ms-stiftung.at/</a></strong>', 0),
(49, 6, 0, 0, '<strong>Impressum</strong><br><strong>ÖMSG gemeinnützige Privatstiftung (MS-Stiftung Österreich)<br><a href="http://www.ms-stiftung.at/">http://www.ms-stiftung.at/</a></strong>', 0),
(57, 1, 0, 0, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 2),
(58, 1, 1, 190, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2),
(66, 2, 1, 190, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2),
(65, 2, 0, 0, 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 2);

-- --------------------------------------------------------

--
-- Table structure for table `module_video`
--

DROP TABLE IF EXISTS `module_video`;
CREATE TABLE IF NOT EXISTS `module_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `top` int(11) NOT NULL,
  `code` varchar(30) NOT NULL,
  `start` int(11) NOT NULL,
  `item_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `module_video`
--

INSERT INTO `module_video` (`id`, `item_id`, `column_id`, `top`, `code`, `start`, `item_type`) VALUES
(13, 1, 1, 0, 'lJFdGEGnR8c', 0, 0),
(12, 2, 0, 100, 'c5xeDi8BSfE', 0, 0),
(11, 2, 0, 640, 'yoyolTHgpEc', 0, 0),
(10, 2, 0, 1120, '_kdKvXoEXfA', 0, 0),
(9, 4, 1, 0, 'c5xeDi8BSfE', 0, 0),
(14, 1, 0, 370, '66ZlQ1m4Fcg', 0, 2),
(18, 2, 0, 370, 'Vm2DchNKpNU', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `teaser_text` text NOT NULL,
  `teaser_image` varchar(255) NOT NULL,
  `prettyurl` varchar(255) NOT NULL,
  `detail_img` varchar(255) DEFAULT NULL,
  `header` text CHARACTER SET latin1,
  `detail_type` int(11) DEFAULT NULL,
  `detail_html` text CHARACTER SET latin1,
  `detail_html_mobile` text CHARACTER SET latin1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `created_date`, `teaser_text`, `teaser_image`, `prettyurl`, `detail_img`, `header`, `detail_type`, `detail_html`, `detail_html_mobile`) VALUES
(1, 'Test article 1', '2016-02-09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '1457353662_GS0zL5aAudh.jpg', 'test-article-1', '1457354539_7TT2yRiI4f.jpg', 'News article 1', 0, '', ''),
(2, 'Test article 2', '2016-02-09', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '1457354799_sEp3vgeceTZD.jpg', 'test-article-2', '1457354825_dZlUmvOuh27d.jpg', 'News article 2', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_header_img` varchar(255) NOT NULL,
  `home_header_img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `blog_header_img`, `home_header_img`) VALUES
(1, 'blogs_header.png', 'home_header.png');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `pword` varchar(255) NOT NULL,
  `valid` int(11) NOT NULL,
  `collection_admin` int(11) DEFAULT '0',
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `country_id`, `username`, `firstname`, `lastname`, `email`, `pword`, `valid`, `collection_admin`, `modified_by`, `modified_date`) VALUES
(1, 1, 'admin', 'admin', 'admin', 'developer@istvanszilagyi.com', '$2a$08$f50yNtNIulvEnSCoKKOzAeZGpIo1WD9OjS58z6vh/58Qqdi6s1B6y', 1, 1, 1, '2015-01-14 10:27:15');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
