<?php
defined('BASEPATH') OR exit('No direct script access allowed');


define('LANG_EN', 0);
define('LANG_DE', 1);

$config['test'] = "";

define('ITEM_ID_BLOGS', -1);
define('ITEM_ID_NEWS', -2);
define('ITEM_ID_PARTNERS', -3);

define('DETAIL_TYPE_IMAGE', 0);
define('DETAIL_TYPE_HTML', 1);

define('ITEM_TYPE_ARTICLE', 0);
define('ITEM_TYPE_BLOG', 1);
define('ITEM_TYPE_NEWS', 2);
define('ITEM_TYPE_PARTNER', 3);
define('ITEM_TYPE_STATICBLOG', 4);

define('MENU_NEWS', 1);
define('MENU_ABOUT_FOUNDATION', 2);
define('MENU_ABOUT_INITIATIVE', 3);
define('MENU_ABOUT_MS', 4);
define('MENU_BLOGS', 5);
define('MENU_EXPERTS', 6);
define('MENU_WORKING_WITH_MS', 7);
define('MENU_CONTACT', 8);
define('MENU_IMPRESS', 9);
define('MENU_DATAPROTECTION', 10);
define('MENU_BEHINDTHECURTAIN', 11);


define('SLIDER_HOME', 0);
define('SLIDER_BLOG', 1);

define('MOSAIC_TYPE_NEWS', 0);
define('MOSAIC_TYPE_VIDEO', 1);
define('MOSAIC_TYPE_ARTICLE', 2);
define('MOSAIC_TYPE_BLOG', 3);

define('BLOG_OVERVIEW', -1);
define('NEWS_OVERVIEW', -1);
define('PARTNER_OVERVIEW', -1);