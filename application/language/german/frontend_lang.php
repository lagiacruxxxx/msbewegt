<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/***********************************************************************************************
 * VARIABLES
 ***********************************************************************************************/


/***********************************************************************************************
 * STRINGS
 ***********************************************************************************************/
$lang['menu_button'] = "MENÜ";
$lang['menu_search_placeholder'] = "Suchen";


$lang['footer_impress'] = "Impressum";
$lang['footer_datasecurity'] = "Datenschutz";
$lang['footer_contact'] = "Kontakt";
$lang['footer_copyright'] = "© 2016";
$lang['footer_text'] = "Eine Initiative der ÖMSG gemeinnützige Privatstiftung (MS-Stiftung Österreich)";

$lang['readmore'] = "MEHR ERFAHREN";
$lang['news_readmore'] = "weiter lesen";

$lang['mobile_menu'] = "MENÜ";