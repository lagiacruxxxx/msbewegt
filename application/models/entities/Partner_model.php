<?php

class Partner_model extends CI_Model
{

    
    function getPartnerById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('partner');
    }
    
    function updateItemData($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('partner', $data);
    }
    
    function clonePartner($data)
    {
        $this->db->insert('partner', $data);
        return $this->db->insert_id();
    }
    
    function getPartner()
    {
        return $this->db->get('partner');
    }
    
}

?>