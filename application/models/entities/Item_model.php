<?php

class Item_model extends CI_Model
{
    function getMetatagsByContinent($continentId)
    {
        $this->db->where('continent_id', $continentId);
        return $this->db->get('metatag');
    }
    
    
    
    function updateItemData($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('item', $data);
    }
    
    function getItem($itemId)
    {
        $this->db->where('id', $itemId);
        return $this->db->get('item');
    }
    
    function getItems()
    {
        $this->db->order_by('name', 'asc');
        return $this->db->get('item');
    }
    

    
    
    
    
    function deleteModulesText($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        $this->db->delete('module_text');
    }
    
    function insertModuleText($data)
    {
        $this->db->insert('module_text', $data);
    }
    
    function getModulesText($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        return $this->db->get('module_text');
    }
    
    function deleteModulesImage($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        $this->db->delete('module_image');
    }
    
    function insertModuleImage($data)
    {
        $this->db->insert('module_image', $data);
    }
    
    function getModulesImage($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        return $this->db->get('module_image');
    }
    
    function deleteModulesBulletpoint($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        $this->db->delete('module_bulletpoint');
    }
    
    function insertModuleBulletpoint($data)
    {
        $this->db->insert('module_bulletpoint', $data);
    }
    
    function getModulesBulletpoint($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        return $this->db->get('module_bulletpoint');
    }    
    
    function deleteModulesVideo($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        $this->db->delete('module_video');
    }
    
    function insertModuleVideo($data)
    {
        $this->db->insert('module_video', $data);
    }
    
    function getModulesVideo($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        return $this->db->get('module_video');
    }
    
    function deleteModulesHTML($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        $this->db->delete('module_html');
    }
    
    function insertModuleHTML($data)
    {
        $this->db->insert('module_html', $data);
    }
    
    function getModulesHTML($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        return $this->db->get('module_html');
    }
    
    function deleteModulesHeadline($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        $this->db->delete('module_headline');
    }
    
    function insertModuleHeadline($data)
    {
        $this->db->insert('module_headline', $data);
    }
    
    function getModulesHeadline($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        return $this->db->get('module_headline');
    }
        
    
    function deleteModulesDownload($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        $this->db->delete('module_download');
    }
    
    function insertModuleDownload($data)
    {
        $this->db->insert('module_download', $data);
    }
    
    function getModulesDownload($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        return $this->db->get('module_download');
    }    
    
    
    
    
    function deleteGalleryItems($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        $this->db->delete('item_galleryitem');
    }
    
    function insertGalleryItems($data)
    {
        $this->db->insert_batch('item_galleryitem', $data);
    }
    
    function getGalleryItems($itemId, $itemType)
    {
        $this->db->where('item_id', $itemId);
        $this->db->where('item_type', $itemType);
        return $this->db->get('item_galleryitem');
    }  
    
    
    
    function cloneItem($data)
    {
        $this->db->insert('item', $data);
        return $this->db->insert_id();
    }
    
    function getMetatagsByItemId($itemId)
    {
        $this->db->where('item_id', $itemId);
        return $this->db->get('metatag_item');
    }
    
    function insertMetatags($data)
    {
        $this->db->insert_batch('metatag_item', $data);
    }
    
    
    function updateItemImage($itemId, $data)
    {
        $this->db->where('id', $itemId);
        $this->db->update('item', $data);
    }
    
    
    function getOrigItemsByMetatag($metatagId)
    {
        $this->db->select('item.id, item.name, item.detail_img');
        $this->db->from('metatag_item');
        $this->db->where('metatag_id', $metatagId);
        $this->db->where('(parent_item_id IS NULL OR parent_item_id = 0)');
        $this->db->join('item', 'item.id = metatag_item.item_id');
        $this->db->order_by('item.name', 'asc');
        return $this->db->get();        
    }
}

?>