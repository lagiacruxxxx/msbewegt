<?php

class News_model extends CI_Model
{

    
    function getNewsById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('news');
    }
    
    function updateItemData($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('news', $data);
    }
    
    function cloneNews($data)
    {
        $this->db->insert('news', $data);
        return $this->db->insert_id();
    }
    
    function getNews()
    {
        return $this->db->get('news');
    }
    
}

?>