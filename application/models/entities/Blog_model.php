<?php

class Blog_model extends CI_Model
{
    function getBloggers()
    {
        return $this->db->get('blogger');
    }
    
    function getBlogEntryById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('blog_entry');
    }
    
    function updateItemData($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('blog_entry', $data);
    }
    
    function cloneBlogEntry($data)
    {
        $this->db->insert('blog_entry', $data);
        return $this->db->insert_id();
    }
    
    function getBlogEntries()
    {
        return $this->db->get('blog_entry');
    }
}

?>