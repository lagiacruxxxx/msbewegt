<?php

class Frontend_model extends CI_Model  
{
    function getMenuitems()
    {
        $this->db->order_by('ordering');
        return $this->db->get('menuitem');
    }
    
    function getBloggers()
    {
        $this->db->order_by('ordering');        
        return $this->db->get('blogger');
    }
    
    function getBlogEntries()
    {
        $this->db->select('blog_entry.*, blogger.entry_header, blogger.name as bloggername');
        $this->db->order_by('created_date', 'desc');
        $this->db->join('blogger', 'blogger.id = blog_entry.blogger_id');
        return $this->db->get('blog_entry');
    }
    
    function getBloggerById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('blogger');
    }
    
    
    
    function getPartners()
    {
        $this->db->order_by('ordering', 'asc');
        return $this->db->get('partner');
    }
    function getPartnerByPrettyUrl($pretty)
    {
        $this->db->where('prettyurl', $pretty);
        return $this->db->get('partner');
    }
    
    
    
    function getItemById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('item');
    }
    
    function getItemByPrettyUrl($pretty)
    {
        $this->db->where('prettyurl', $pretty);
        return $this->db->get('item');
    }
    function getBlogEntryByPrettyUrl($pretty)
    {
        $this->db->where('prettyurl', $pretty);
        return $this->db->get('blog_entry');
    }
    
    function getBlogEntryByID($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('blog_entry');
    }
    
    
    
    
    
    function getModuleTextByItemId($itemId, $itemType)
    {
        $this->db->select('*, "text" as "mod"');
        $this->db->where('item_type', $itemType);
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_text');
    }
    
    function getModuleImageByItemId($itemId, $itemType)
    {
        $this->db->select('*, "image" as "mod"');
        $this->db->where('item_type', $itemType);
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_image');
    }
    
    function getModuleVideoByItemId($itemId, $itemType)
    {
        $this->db->select('*, "video" as "mod"');
        $this->db->where('item_type', $itemType);
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_video');
    }
    
    function getModuleHTMLByItemId($itemId, $itemType)
    {
        $this->db->select('*, "html" as "mod"');
        $this->db->where('item_type', $itemType);
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_html');
    }
    
    function getModuleDownloadByItemId($itemId, $itemType)
    {
        $this->db->select('*, "download" as "mod"');
        $this->db->where('item_type', $itemType);
        $this->db->where('item_id', $itemId);
        return $this->db->get('module_download');
    }
    
    
    
    function getNews()
    {
        $this->db->order_by('created_date', 'desc');
        return $this->db->get('news');
    }
    
    function getNewsByPrettyUrl($pretty)
    {
        $this->db->where('prettyurl', $pretty);
        return $this->db->get('news');
    }
    
    function getNewsById($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('news');
    }
    
    
    
    function getSettings()
    {
        return $this->db->get('settings');
    }
    
    function getSliders($slider_type)
    {
        $this->db->where('slider_type', $slider_type);
        $this->db->order_by('ordering', 'asc');
        return $this->db->get('slider');
    }
    
    
    
    function search_news($input)
    {
        $this->db->like('teaser_text', $input);
        $this->db->or_like('header', $input);
        return $this->db->get('news');
    }
    
    function search_items($input)
    {
        $this->db->like('name', $input);
        $this->db->or_like('header', $input);
        return $this->db->get('item');
    }
    
    function search_blogs($input)
    {
        $this->db->like('header', $input);
        $this->db->or_like('teaser_text', $input);
        return $this->db->get('blog_entry');
    }
    
    function getFirstTextModule($id, $type)
    {
        $this->db->where('column_id', 0);
        $this->db->where('item_type', $type);
        $this->db->where('item_id', $id);
        $this->db->order_by('top', 'asc');
        return $this->db->get('module_text');
    }
    
    function getTextModules($input)
    {
        $this->db->like('content', $input);
        return $this->db->get('module_text');
    }
    
    
    
    function getMosaic()
    {
        $this->db->where('show', 1);
        $this->db->order_by('ordering');
        return $this->db->get('mosaic');
    }
    
}
