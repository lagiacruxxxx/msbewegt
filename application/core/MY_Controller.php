<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller 
{
    protected $language = NULL;
    protected $is_mobile = NULL;
    protected $is_ipad = NULL;
    	
    function __construct()
    {
        parent::__construct();

        $this->load->helper('besc_helper');
            
        $this->_connectDB();
        $this->load->library('session');
        $this->detectDevice();
        date_default_timezone_set('Europe/Vienna');
        
        $this->config->load('msbewegt');
       
    }  

    private function _connectDB()
    {
        if( strpos(site_url(), 'localhost') !== false || (strpos(site_url(), '192.168.1') !== false && strpos(site_url(), '192.168.1.7') === false) )
            $this->load->database('development');
        elseif(strpos(site_url(), 'fileserver') !== false || strpos(site_url(), '192.168.1.7') !== false)
            $this->load->database('testing');
        else
            $this->load->database('productive');
    }

    protected function logged_in()
    {
        return (bool) $this->session->userdata('user_id');
    }
    
    protected function _setLang()
    {
        $this->load->helper('cookie');
        $cookie_lang = get_cookie('tba21_lang');
        if($cookie_lang == null)
            $this->language = LANG_EN;
        else 
            $this->language = $cookie_lang;
        
        if($this->language == LANG_DE)
            $this->lang->load('frontend', 'german');
        else
            $this->lang->load('frontend', 'english');
    }
    
    protected function detectDevice()
    {
        $this->is_ipad = $this->agent->is_mobile('ipad');
        
        if($this->agent->is_mobile() && !$this->is_ipad)
            $this->is_mobile = true;
        else
            $this->is_mobile = false;
    }
}
