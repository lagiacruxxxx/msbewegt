<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Mosaic extends Backend 
{
    function __construct()
    {
        parent::__construct();
    }  

	public function items()
	{
		$bc = new besc_crud();
		$bc->table('mosaic');
		$bc->primary_key('id');
		$bc->title('Mosaic');
		
		$bc->list_columns(array(
		    'name', 'item_type', 'show', 
		));
		
		
		$bc->ordering(array(
		    'ordering' => 'ordering',
		    'value' => 'name',
		));
		$bc->filter_columns(array('name', 'item_type'));
		
		$bc->unset_add();
		$bc->unset_edit();
		
		$bc->custom_actions(array(
            array(
                'name' => 'Add News mosaic (small)',
                'url' => site_url('entities/Mosaic/NewsMosaic/add'),
                'add_pk' => false,    
                'icon' => site_url('items/backend/img/icon_news.png'),
            ),    

		    array(
		        'name' => 'Add Article mosaic (small)',
		        'url' => site_url('entities/Mosaic/ArticleMosaic/add'),
		        'add_pk' => false,
		        'icon' => site_url('items/backend/img/icon_article.png'),
		    ),
		    
		    array(
		        'name' => 'Add Image mosaic (big)',
		        'url' => site_url('entities/Mosaic/VideoMosaic/add'),
		        'add_pk' => false,
		        'icon' => site_url('items/backend/img/icon_video.png'),
		    ),
		    
		    array(
		        'name' => 'Add Blog mosaic (big)',
		        'url' => site_url('entities/Mosaic/BlogMosaic/add'),
		        'add_pk' => false,
		        'icon' => site_url('items/backend/img/icon_blog.png'),
		    ),
		    
		));
		
		$bc->custom_buttons(array(
		    array(
		        'name' => 'Edit',
		        'icon' => site_url('items/besc_crud/img/edit.png'),
		        'add_pk' => true,
		        'url' => site_url('entities/Mosaic/editMosaic')),
		));
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Name',
	            'validation' => 'required',
	        ),
	        
	        'item_type' => array
	        (
	            'db_name' => 'item_type',
	            'type' => 'select',
	            'display_as' => 'Type',
	            'options' => array(
	                array(
	                    'key' => MOSAIC_TYPE_ARTICLE,
	                    'value' => 'Article (small)'
	                ),
	                array(
	                    'key' => MOSAIC_TYPE_NEWS,
	                    'value' => 'News (small)',
	                ),
	                
	                array(
	                    'key' => MOSAIC_TYPE_VIDEO,
	                    'value' => 'Image (big)',
	                ),
	                
	                array(
	                    'key' => MOSAIC_TYPE_BLOG,
	                    'value' => 'Blog (big)',
	                ),	                
	            ),
	        ),	        
	        
	        'show' => array
	        (
	            'db_name' => 'show',
	            'type' => 'select',
	            'display_as' => 'Show in frontend',
	            'options' => array(
	                array(
                        'key' => 1,
	                    'value' => 'Show'   
	                ),
	                array(
	                    'key' => 0,
	                    'value' => 'Hide',
	                )
	            ),
	        ),
	        
	        
	        
	    ));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);
	}
	
	public function editMosaic($mosaicId)
	{
	    $this->load->model('entities/Mosaic_model', 'mm');
	    switch($this->mm->getMosaicById($mosaicId)->row()->item_type)
	    {
	        case MOSAIC_TYPE_NEWS:
	            redirect(site_url('entities/Mosaic/NewsMosaic/edit/' . $mosaicId));
	            break;
            case MOSAIC_TYPE_ARTICLE:
                redirect(site_url('entities/Mosaic/ArticleMosaic/edit/' . $mosaicId));
                break;	       
            case MOSAIC_TYPE_VIDEO:
                redirect(site_url('entities/Mosaic/VideoMosaic/edit/' . $mosaicId));
                break;
            case MOSAIC_TYPE_BLOG:
                redirect(site_url('entities/Mosaic/BlogMosaic/edit/' . $mosaicId));
                break;                
	    }
	    
	}
	
	
	
	public function NewsMosaic()
	{
	    if($this->uri->segment_array()[4] == 'list' || !isset($this->uri->segment_array()[4]))
            redirect(site_url('entities/Mosaic/items'));
	    
	    $bc = new besc_crud();
	    $bc->table('mosaic');
	    $bc->primary_key('id');
	    $bc->title('News mosaic');
	    
	    $this->load->model('entities/News_model', 'nm');
	    
	    $itemOptions = array();
	    $itemOptions[] = array(
	        'key' => NEWS_OVERVIEW,
	        'value' => 'Overview',
	    );
	    foreach($this->nm->getNews()->result() as $news)
	    {
	        $itemOptions[] = array(
	            'key' => $news->id,
                'value' => $news->header,  
	        );
	    }
	    
	    $bc->unset_delete();
	    
	    $bc->columns(array
        (
            'name' => array
            (
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
                'validation' => 'required',
            ),
             
            'item_id' => array
            (
                'db_name' => 'item_id',
                'type' => 'select',
                'display_as' => 'News',
                'options' => $itemOptions,
                'validation' => 'required',
            ),
            
            'teaser_text' => array(
                'db_name' => 'teaser_text',
                'type' => 'text',
                'display_as' => 'Teaser text',
                'validation' => 'required|max_length[200]',
            ),
             
            'teaser_image' => array(
                'db_name' => 'teaser_image',
                'type' => 'image',
                'display_as' => 'Teaser image',
                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>280x160 px',
                'accept' => '.png,.jpg,.jpeg',
                'uploadpath' => 'items/uploads/mosaic',
                'crop' => array(
                    'ratio' => '280:160',
                    'minWidth' => 280,
                    'minHeight' => 160,
                    'maxWidth' => 560,
                    'maxHeight' => 320,
                ),
            ),
            
             
            'show' => array
            (
                'db_name' => 'show',
                'type' => 'select',
                'display_as' => 'Show in frontend',
                'options' => array(
                    array(
                        'key' => 1,
                        'value' => 'Show'
                    ),
                    array(
                        'key' => 0,
                        'value' => 'Hide',
                    )
                ),
            ),
            
            'item_type' => array
            (
                'db_name' => 'item_type',
                'type' => 'hidden',
                'value' => MOSAIC_TYPE_NEWS,
            ),
             
        ));
	    
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	
	
	
	public function ArticleMosaic()
	{
	    if($this->uri->segment_array()[4] == 'list' || !isset($this->uri->segment_array()[4]))
	        redirect(site_url('entities/Mosaic/items'));
	     
	    $bc = new besc_crud();
	    $bc->table('mosaic');
	    $bc->primary_key('id');
	    $bc->title('Article mosaic');
	     
	    $this->load->model('entities/Item_model', 'em');
	     
	    $itemOptions = array();
	    $itemOptions[] = array(
	        'key' => PARTNER_OVERVIEW,
	        'value' => 'Vor den Vorhang',
	    );
	    foreach($this->em->getItems()->result() as $item)
	    {
	        $itemOptions[] = array(
	            'key' => $item->id,
	            'value' => $item->header,
	        );
	    }
	    
	    $colors = array(
	        array(
	            'key' => '#ffffff',
	            'value' => 'White'
	        ),
	        array(
	            'key' => '#000000',
	            'value' => 'Black'
	        ),
	        array(
	            'key' => '#60c0d0',
	            'value' => 'Turquoise'
	        ),
	    );
	     
	    $bc->unset_delete();
	     
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	             
	            'item_id' => array
	            (
	                'db_name' => 'item_id',
	                'type' => 'select',
	                'display_as' => 'Article',
	                'options' => $itemOptions,
	                'validation' => 'required',
	            ),
	
	            'teaser_text' => array(
	                'db_name' => 'teaser_text',
	                'type' => 'text',
	                'display_as' => 'Teaser text',
	                'validation' => 'required|max_length[200]',
	            ),
	             
	            'teaser_image' => array(
	                'db_name' => 'teaser_image',
	                'type' => 'image',
	                'display_as' => 'Teaser image',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>280x160 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/mosaic',
                    'crop' => array(
                        'ratio' => '280:160',
                        'minWidth' => 280,
                        'minHeight' => 160,
                        'maxWidth' => 560,
                        'maxHeight' => 320,
                    ),	                
	            ),
	
	             
	            'show' => array
	            (
	                'db_name' => 'show',
	                'type' => 'select',
	                'display_as' => 'Show in frontend',
	                'options' => array(
	                    array(
	                        'key' => 1,
	                        'value' => 'Show'
	                    ),
	                    array(
	                        'key' => 0,
	                        'value' => 'Hide',
	                    )
	                ),
	            ),
	            
	            'item_type' => array
	            (
	                'db_name' => 'item_type',
	                'type' => 'hidden',
	                'value' => MOSAIC_TYPE_ARTICLE,
	            ),
	            
	             
	        ));
	     
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	
	public function VideoMosaic()
	{
	    if($this->uri->segment_array()[4] == 'list' || !isset($this->uri->segment_array()[4]))
	        redirect(site_url('entities/Mosaic/items'));
	
	    $bc = new besc_crud();
	    $bc->table('mosaic');
	    $bc->primary_key('id');
	    $bc->title('Video mosaic');
	
	    $this->load->model('entities/Item_model', 'em');
	
	    $itemOptions = array();
	    foreach($this->em->getItems()->result() as $item)
	    {
	        $itemOptions[] = array(
	            'key' => $item->id,
	            'value' => $item->header,
	        );
	    }
	    
	    $colors = array(
	        array(
	            'key' => '#ffffff',
	            'value' => 'White'
	        ),
	        array(
	            'key' => '#000000',
	            'value' => 'Black'
	        ),
	        array(
	            'key' => '#60c0d0',
	            'value' => 'Turquoise'
	        ),
	    );
	
	    $bc->unset_delete();
	
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	
	            
	            'item_id' => array
	            (
	                'db_name' => 'item_id',
	                'type' => 'select',
	                'display_as' => 'Article',
	                'options' => $itemOptions,
	                'validation' => 'required',
	            ),
	
	            'teaser_text' => array(
	                'db_name' => 'teaser_text',
	                'type' => 'text',
	                'display_as' => 'Teaser text',
	                'validation' => 'required|max_length[200]',
	            ),
	            
                'teaser_image' => array(
	                'db_name' => 'teaser_image',
	                'type' => 'image',
	                'display_as' => 'Teaser image',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>570x373 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/mosaic',
                    'crop' => array(
                        'ratio' => '280:160',
                        'minWidth' => 280,
                        'minHeight' => 160,
                        'maxWidth' => 560,
                        'maxHeight' => 320,
                    ),
	            ),
	
	
	            'show' => array
	            (
	                'db_name' => 'show',
	                'type' => 'select',
	                'display_as' => 'Show in frontend',
	                'options' => array(
	                    array(
	                        'key' => 1,
	                        'value' => 'Show'
	                    ),
	                    array(
	                        'key' => 0,
	                        'value' => 'Hide',
	                    )
	                ),
	            ),
	            
	            'textcolor' => array(
	                'db_name' => 'textcolor',
	                'type' => 'select',
	                'display_as' => 'Text color',
	                'options' => $colors,
	            ),
	            
	            
	            'item_type' => array
	            (
	                'db_name' => 'item_type',
	                'type' => 'hidden',
	                'value' => MOSAIC_TYPE_VIDEO,
	            ),
	            
	
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	
	
	public function BlogMosaic()
	{
	    if($this->uri->segment_array()[4] == 'list' || !isset($this->uri->segment_array()[4]))
	        redirect(site_url('entities/Mosaic/items'));
	
	    $bc = new besc_crud();
	    $bc->table('mosaic');
	    $bc->primary_key('id');
	    $bc->title('Blog mosaic');
	
	    $this->load->model('entities/Blog_model', 'em');
	
	    $itemOptions = array();
	    $itemOptions[] = array(
            'key' => BLOG_OVERVIEW,
	        'value' => 'Overview',
	    );
	    foreach($this->em->getBlogEntries()->result() as $item)
	    {
	        $itemOptions[] = array(
	            'key' => $item->id,
	            'value' => $item->header,
	        );
	    }
	    
	    $colors = array(
	        array(
	            'key' => '#ffffff',
	            'value' => 'White'
	        ),
	        array(
	            'key' => '#000000',
	            'value' => 'Black'
	        ),
	        array(
	            'key' => '#60c0d0',
	            'value' => 'Turquoise'
	        ),
	    );
	
	    $bc->unset_delete();
	
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	
	            
	            'item_id' => array
	            (
	                'db_name' => 'item_id',
	                'type' => 'select',
	                'display_as' => 'Blog',
	                'options' => $itemOptions,
	                'validation' => 'required',
	            ),
	
	            'teaser_text' => array(
	                'db_name' => 'teaser_text',
	                'type' => 'text',
	                'display_as' => 'Teaser text',
	                'validation' => 'required|max_length[200]',
	            ),
	            
	            'teaser_text2' => array(
	                'db_name' => 'teaser_text2',
	                'type' => 'text',
	                'display_as' => 'Teaser subtext',
	                'validation' => 'required|max_length[200]',
	            ),
	             
	            'teaser_image' => array(
	                'db_name' => 'teaser_image',
	                'type' => 'image',
	                'display_as' => 'Teaser image',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>570x373 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/mosaic',
	                'crop' => array(
	                    'ratio' => '280:160',
	                    'minWidth' => 280,
	                    'minHeight' => 160,
	                    'maxWidth' => 560,
	                    'maxHeight' => 320,
	                ),
	            ),
	
	
	            'show' => array
	            (
	                'db_name' => 'show',
	                'type' => 'select',
	                'display_as' => 'Show in frontend',
	                'options' => array(
	                    array(
	                        'key' => 1,
	                        'value' => 'Show'
	                    ),
	                    array(
	                        'key' => 0,
	                        'value' => 'Hide',
	                    )
	                ),
	            ),
	            
	            'item_type' => array
	            (
	                'db_name' => 'item_type',
	                'type' => 'hidden',
	                'value' => MOSAIC_TYPE_BLOG,
	            ),
	            
	
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	

}

