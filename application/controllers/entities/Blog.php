<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Blog extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Blog_model', 'em');
        $this->load->model('entities/Item_model', 'im');
    }  

	public function entries()
	{
		$bc = new besc_crud();
		$bc->table('blog_entry');
		$bc->primary_key('id');
		$bc->title('Blog entries');
		
		$bc->custom_buttons(array(
		    array(
		        'name' => 'Edit blog entry',
                'icon' => site_url('items/backend/img/wrench_green.png'),
                'add_pk' => true,
                'url' => 'edit_blog_entry'),
		    array(
		        'name' => 'Clone blog entry',
		        'icon' => site_url('items/backend/img/Clone-Stamp-Tool-icon.png'),
		        'add_pk' => true,
		        'url' => 'clone_blog_entry'),
		));
		
		$bc->custom_buttons(array());
		$bc->list_columns(array(
		    'name', 'blogger_id', 'teaser', 'created_date',
		));
		
		$bc->order_by_field('created_date');
		$bc->order_by_direction('desc');
		
		$bloggers = array();
        foreach($this->em->getBloggers()->result() as $blogger)
        {
            $bloggers[] = array(
                'key' => $blogger->id,
                'value' => $blogger->name,
            );
        }
		
		$bc->filter_columns(array('name', 'blogger_id'));
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Name',
	            'validation' => 'required',
	        ),
	        
	        'prettyurl' => array
	        (
	            'db_name' => 'prettyurl',
	            'type' => 'text',
	            'display_as' => 'PrettyURL',
	            'validation' => 'required|is_unique[blog_entry.prettyurl]',
	        ),
	        
	        'blogger_id' => array(
	           'db_name' => 'blogger_id',
	            'type' => 'select',
	            'options' => $bloggers,
	            'display_as' => 'Blogger',
	            'validation' =>  'required',
	        ),
	        
	        'teaser_text' => array(
	            'db_name' => 'teaser_text',
	            'type' => 'multiline',
	            'display_as' => 'Teaser text',
	            'validation' => 'required|max_length[300]',
	        ),
	        
	        'created_date' => array(
	            'db_name' => 'created_date',
	            'type' => 'date',
	            'display_as' => 'Date',
	            'edit_format' => 'dd.mm.yy',
	            'list_format' => 'd.m.Y',
	            'defaultvalue' => '01.01.2016',
	        ),
	        
	    ));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);
	}
	
	
	public function edit_blog_entry($itemId)
	{
	    $data = array();
	    $data['itemType'] = ITEM_TYPE_BLOG;
	    $data['item'] = $this->em->getBlogEntryById($itemId)->row();
        
	    $data['modulesText'] = $this->im->getModulesText($itemId, ITEM_TYPE_BLOG);
	    $data['modulesImage'] = $this->im->getModulesImage($itemId, ITEM_TYPE_BLOG);
	    $data['galleryItems'] = $this->im->getGalleryItems($itemId, ITEM_TYPE_BLOG);
	    $data['modulesVideo'] = $this->im->getModulesVideo($itemId, ITEM_TYPE_BLOG);
	    $data['modulesHTML'] = $this->im->getModulesHTML($itemId, ITEM_TYPE_BLOG);
	    $data['modulesDownload'] = $this->im->getModulesDownload($itemId, ITEM_TYPE_BLOG);
	    
	    $crud_data['crud_data'] = $this->load->view('backend/edit_content', $data, true);
	    $this->page('backend/crud', $crud_data);

	}
	
		
	
	public function save_blog_entry()
	{
	    $data = array(
	        'detail_img' => $_POST['detail_img'],
	        'header' => $_POST['header'],
	        'detail_type' => $_POST['detail_type'],
	        'detail_html' => $_POST['detail_html'],
	        'detail_html_mobile' => $_POST['detail_html_mobile']
	    );
	    $this->em->updateItemData($_POST['id'], $data);
	    
	    $this->im->deleteModulesText($_POST['id'], ITEM_TYPE_BLOG);
	    $this->im->deleteModulesImage($_POST['id'], ITEM_TYPE_BLOG);
	    $this->im->deleteModulesVideo($_POST['id'], ITEM_TYPE_BLOG);
	    $this->im->deleteModulesHTML($_POST['id'], ITEM_TYPE_BLOG);
	    $this->im->deleteModulesDownload($_POST['id'], ITEM_TYPE_BLOG);
	    if(isset($_POST['modules']))
	    {
	        foreach($_POST['modules'] as $module)
	        {
	            switch($module['type'])
	            {
	                case 'text':
	                    $this->im->insertModuleText(array(
                            'item_id' => $_POST['id'],
                            'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
                            'top' => $module['top'],
                            'content' => $module['content'],
                            'item_type' => ITEM_TYPE_BLOG,
	                    ));
	                    break;
	                    
	                case 'image':
	                    $this->im->insertModuleImage(array(
	                        'item_id' => $_POST['id'],
	                        'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
	                        'top' => $module['top'],
	                        'fname' => $module['fname'],
	                        'item_type' => ITEM_TYPE_BLOG,
	                    ));
	                    break;
	                    
                    case 'video':
                        $this->im->insertModuleVideo(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
                        'top' => $module['top'],
                        'code' => $module['code'],
                        'start' => $module['start'],
                        'item_type' => ITEM_TYPE_BLOG,
                        ));
                        break;
                        
                    case 'html':
                        $this->im->insertModuleHTML(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
                        'top' => $module['top'],
                        'html' => $module['html'],
                        'item_type' => ITEM_TYPE_BLOG,
                        ));
                        break;
                        
                    case 'download':
                        $array = array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
                        'top' => $module['top'],
                        'fname' => $module['fname'],
                        'description' => $module['description'],
                        'item_type' => ITEM_TYPE_BLOG,
                        );
                        if($array['fname'] == '')
                            unset($array['fname']);
                        $this->im->insertModuleDownload($array);
                        break;                        
	            }
	        }
	    }
	    
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );
	}
	
	
	public function clone_blog_entry($itemId)
	{
	    //clone item
	    $item = $this->em->getBlogEntryById($itemId)->row_array();
	    $item['name'] = $item['name'] . ' (CLONE)';
	    unset($item['id']);
	    $newItemId = $this->em->cloneBlogEntry($item);
	    
	    //html module
	    foreach($this->im->getModulesHTML($itemId, ITEM_TYPE_BLOG)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'html' => $data->html,
	            'item_type' => $data->item_type,
	        );
	        $this->im->insertModuleHTML($batch);
	    }	    
	    
	    //image module
	    foreach($this->im->getModulesImage($itemId, ITEM_TYPE_BLOG)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'fname' => $data->fname,
	            'item_type' => $data->item_type,
	        );
	        $this->im->insertModuleImage($batch);
	    }
	    
	    //text module
	    foreach($this->im->getModulesText($itemId, ITEM_TYPE_BLOG)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'content' => $data->content,
	            'item_type' => $data->item_type,
	        );
	        $this->im->insertModuleText($batch);
	    }
	    
	    //video module
	    foreach($this->im->getModulesVideo($itemId, ITEM_TYPE_BLOG)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'start' => $data->start,
	            'code' => $data->code,
	            'item_type' => $data->item_type,
	        );
	        $this->im->insertModuleVideo($batch);
	    }
	    
	    //download module
	    foreach($this->im->getModulesDownload($itemId, ITEM_TYPE_BLOG)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'fname' => $data->fname,
	            'description' => $data->description,
	            'item_type' => $data->item_type,
	        );
	        $this->im->insertModuleDownload($batch);
	    }
	    
	    redirect('entities/Blog/entries', 'refresh');
	}
	
	
	public function bloggers()
	{
	    $bc = new besc_crud();
	    $bc->table('blogger');
	    $bc->primary_key('id');
	    $bc->title('Blogger');
	
        $bc->custom_buttons(array());
	    $bc->list_columns(array(
	        'name', 'img_active', 
	    ));
	
	    $bc->order_by_field('ordering');
	    $bc->order_by_direction('asc');
	
	    
	    $bc->ordering(array(
	        'ordering' => 'ordering',
	        'value' => 'name',
	    ));
	    
	    $bc->filter_columns(array('name'));
	
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => 'required',
	            ),
	             
	            'desc' => array
	            (
	                'db_name' => 'desc',
	                'type' => 'text',
	                'display_as' => 'Description',
	                'validation' => 'required|max_length[400]',
	            ),
	             
	            'img_active' => array(
    	            'db_name' => 'img_active',
    	            'type' => 'image',
    	            'display_as' => 'Blogger image colored',
    	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>280x120 px',
    	            'accept' => '.png,.jpg,.jpeg',
    	            'uploadpath' => 'items/uploads/bloggers'
    	        ),	

	            'img_inactive' => array(
	                'db_name' => 'img_inactive',
	                'type' => 'image',
	                'display_as' => 'Blogger image grey',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>280x120 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/bloggers'
	            ),
	            
	            'entry_header' => array(
	                'db_name' => 'entry_header',
	                'type' => 'image',
	                'display_as' => 'Blog header',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>278x86 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/bloggers'
	            ),
	             
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
}

