<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Item extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Item_model', 'em');
    }  

	public function items()
	{
		$bc = new besc_crud();
		$bc->table('item');
		$bc->primary_key('id');
		$bc->title('Items');
		
		$bc->custom_buttons(array(
		    array(
		        'name' => 'Edit item',
                'icon' => site_url('items/backend/img/wrench_green.png'),
                'add_pk' => true,
                'url' => 'edit_item'),
		    array(
		        'name' => 'Clone item',
		        'icon' => site_url('items/backend/img/Clone-Stamp-Tool-icon.png'),
		        'add_pk' => true,
		        'url' => 'clone_item'),
		));
		
		$bc->list_columns(array(
		    'name', 'show', 'detail_img',
		));
		
		$bc->filter_columns(array('name'));
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Name',
	            'validation' => 'required',
	        ),
	        
	        'prettyurl' => array
	        (
	            'db_name' => 'prettyurl',
	            'type' => 'text',
	            'display_as' => 'PrettyURL',
	            'validation' => 'required|is_unique[item.prettyurl]',
	        ),
	        
	        'show' => array
	        (
	            'db_name' => 'show',
	            'type' => 'select',
	            'display_as' => 'Show in frontend',
	            'options' => array(
	                array(
                        'key' => 1,
	                    'value' => 'Show'   
	                ),
	                array(
	                    'key' => 0,
	                    'value' => 'Hide',
	                )
	            ),
	        ),
	        
	    ));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);
	}
	
	
	public function edit_item($itemId)
	{
	    $data = array();
	    $data['itemType'] = ITEM_TYPE_ARTICLE;
	    $data['item'] = $this->em->getItem($itemId)->row();
        
	    $data['modulesText'] = $this->em->getModulesText($itemId, ITEM_TYPE_ARTICLE);
	    $data['modulesImage'] = $this->em->getModulesImage($itemId, ITEM_TYPE_ARTICLE);
	    $data['modulesVideo'] = $this->em->getModulesVideo($itemId, ITEM_TYPE_ARTICLE);
	    $data['modulesHTML'] = $this->em->getModulesHTML($itemId, ITEM_TYPE_ARTICLE);
	    $data['modulesDownload'] = $this->em->getModulesDownload($itemId, ITEM_TYPE_ARTICLE);
	    
	    $crud_data['crud_data'] = $this->load->view('backend/edit_content', $data, true);
	    $this->page('backend/crud', $crud_data);
	    
	}
	
	public function save_item()
	{
	    $data = array(
	        'detail_img' => $_POST['detail_img'],
	        'header' => $_POST['header'],
	        'detail_type' => $_POST['detail_type'],
	        'detail_html' => $_POST['detail_html'],
	        'detail_html_mobile' => $_POST['detail_html_mobile']
	    );
	    $this->em->updateItemData($_POST['id'], $data);
	    
	    $this->em->deleteModulesText($_POST['id'], ITEM_TYPE_ARTICLE);
	    $this->em->deleteModulesImage($_POST['id'], ITEM_TYPE_ARTICLE);
	    $this->em->deleteModulesVideo($_POST['id'], ITEM_TYPE_ARTICLE);
	    $this->em->deleteModulesHTML($_POST['id'], ITEM_TYPE_ARTICLE);
	    $this->em->deleteModulesDownload($_POST['id'], ITEM_TYPE_ARTICLE);
	    if(isset($_POST['modules']))
	    {
	        foreach($_POST['modules'] as $module)
	        {
	            switch($module['type'])
	            {
	                case 'text':
	                    $this->em->insertModuleText(array(
                            'item_id' => $_POST['id'],
                            'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
                            'top' => $module['top'],
                            'content' => $module['content'],
                            'item_type' => ITEM_TYPE_ARTICLE,
	                    ));
	                    break;
	                    
	                case 'image':
	                    $this->em->insertModuleImage(array(
	                        'item_id' => $_POST['id'],
	                        'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
	                        'top' => $module['top'],
	                        'fname' => $module['fname'],
	                        'item_type' => ITEM_TYPE_ARTICLE,
	                    ));
	                    break;
	                    
                    case 'video':
                        $this->em->insertModuleVideo(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
                        'top' => $module['top'],
                        'code' => $module['code'],
                        'start' => $module['start'],
                        'item_type' => ITEM_TYPE_ARTICLE,
                        ));
                        break;
                        
                    case 'html':
                        $this->em->insertModuleHTML(array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
                        'top' => $module['top'],
                        'html' => $module['html'],
                        'item_type' => ITEM_TYPE_ARTICLE,
                        ));
                        break;
                        
                    case 'download':
                        $array = array(
                        'item_id' => $_POST['id'],
                        'column_id' => ($module['column'] == 'itemcol_big') ? 0 : 1,
                        'top' => $module['top'],
                        'fname' => $module['fname'],
                        'description' => $module['description'],
                        'item_type' => ITEM_TYPE_ARTICLE,
                        );
                        if($array['fname'] == '')
                            unset($array['fname']);
                        $this->em->insertModuleDownload($array);
                        break;                        
	            }
	        }
	    }
	    
	    echo json_encode(
	        array(
	            'success' => true,
	        )
	    );
	}
	
	
	public function clone_item($itemId)
	{
	    //clone item
	    $item = $this->em->getItem($itemId)->row_array();
	    $item['name'] = $item['name'] . ' (CLONE)';
	    unset($item['id']);
	    $newItemId = $this->em->cloneItem($item);
	    
	    
	    
	    
	    //html module
	    foreach($this->em->getModulesHTML($itemId, ITEM_TYPE_ARTICLE)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'html' => $data->html,
	            'item_type' => $data->item_type,
	        );
	        $this->em->insertModuleHTML($batch);
	    }	    
	    
	    //image module
	    foreach($this->em->getModulesImage($itemId, ITEM_TYPE_ARTICLE)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'fname' => $data->fname,
	            'item_type' => $data->item_type,
	        );
	        $this->em->insertModuleImage($batch);
	    }
	    
	    //text module
	    foreach($this->em->getModulesText($itemId, ITEM_TYPE_ARTICLE)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'content' => $data->content,
	            'item_type' => $data->item_type,
	        );
	        $this->em->insertModuleText($batch);
	    }
	    
	    //video module
	    foreach($this->em->getModulesVideo($itemId, ITEM_TYPE_ARTICLE)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'start' => $data->start,
	            'code' => $data->code,
	            'item_type' => $data->item_type,
	        );
	        $this->em->insertModuleVideo($batch);
	    }
	    
	    //download module
	    foreach($this->em->getModulesDownload($itemId, ITEM_TYPE_ARTICLE)->result() as $data)
	    {
	        $batch = array(
	            'item_id' => $newItemId,
	            'column_id' => $data->column_id,
	            'top' => $data->top,
	            'fname' => $data->fname,
	            'description' => $data->description,
	            'item_type' => $data->item_type,
	        );
	        $this->em->insertModuleDownload($batch);
	    }
	    	    
	    redirect('entities/item/items', 'refresh');
	}

}

