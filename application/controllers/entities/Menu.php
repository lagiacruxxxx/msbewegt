<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Menu extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Menu_model', 'em');
    }  

	public function mainmenu()
	{
		$bc = new besc_crud();
		$bc->table('menuitem');
		$bc->primary_key('id');
		$bc->title('Mainmenu item');
		
		$bc->list_columns(array(
		    'name', 'item_id'
		));
		
		/*$bc->ordering(array(
		    'ordering' => 'ordering',
		    'value' => 'name',
		));*/
		
		$items = array();
		$items[] = array(
		    'key' => ITEM_ID_BLOGS,
		    'value' => 'Blogs overview',
		);
		$items[] = array(
		    'key' => ITEM_ID_NEWS,
		    'value' => 'News list',
		);
		$items[] = array(
		    'key' => ITEM_ID_PARTNERS,
		    'value' => 'Partners',
		);
		foreach($this->em->getItems()->result() as $item)
		{
		    $items[] = array(
		        'key' => $item->id,
		        'value' => $item->name,
		    );
		}
		
		$bc->unset_add();
		
		$bc->columns(array
	    (
	        'name' => array(
                'db_name' => 'name',
	            'type' => 'text',
	            'display_as' => 'Name',
	            'validation' => 'required',
	        ),
	        
	        'item_id' => array(
                'db_name' => 'item_id',
                'type' => 'combobox',
                'display_as' => 'Item',
                'options' => $items,
                'validation' => '',
	            'width' => 150,
	        )
	        
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
}
