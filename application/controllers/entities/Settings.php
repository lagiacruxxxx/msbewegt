<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/Backend.php');

class Settings extends Backend 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('entities/Blog_model', 'em');
        $this->load->model('entities/Item_model', 'im');
    }  

	/*public function settings()
	{
		$bc = new besc_crud();
		$bc->table('settings');
		$bc->primary_key('id');
		$bc->title('Settings');
		
		$bc->custom_buttons(array());
		$bc->list_columns(array(
		    'blog_header_img', 'home_header_img',
		));
		
		$bc->unset_add();
		$bc->unset_delete();
		
		$bc->columns(array
	    (
	        'blog_header_img' => array(
	            'db_name' => 'blog_header_img',
	            'type' => 'image',
	            'display_as' => 'Blog site header image',
	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>1000px width',
	            'accept' => '.png,.jpg,.jpeg',
	            'uploadpath' => 'items/uploads/header'
	        ),	
	        
	        'home_header_img' => array(
	            'db_name' => 'home_header_img',
	            'type' => 'image',
	            'display_as' => 'Home header image',
	            'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>1000px width',
	            'accept' => '.png,.jpg,.jpeg',
	            'uploadpath' => 'items/uploads/header'
	        ),	
	        
	    ));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);
	}*/
	
	
	public function home_slider()
	{
	    $bc = new besc_crud();
	    $bc->table('slider');
	    $bc->primary_key('id');
	    $bc->title('Home Slider');
	    $bc->where('slider_type = ' . SLIDER_HOME);
	
	    $bc->custom_buttons(array());
	    $bc->list_columns(array(
	        'fname',
	    ));
	    
	    $bc->ordering(array(
	        'ordering' => 'ordering',
	        'value' => 'name',
	    ));
	
	
	    $bc->columns(array
        (
            'name' => array
            (
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
                'validation' => '',
            ),
             
            'fname' => array(
                'db_name' => 'fname',
                'type' => 'image',
                'display_as' => 'Slider image',
                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>1000 x 405 px',
                'accept' => '.png,.jpg,.jpeg',
                'uploadpath' => 'items/uploads/slider'
            ),
            
            'continent_id' => array(
                'db_name' => 'slider_type',
                'type' => 'hidden',
                'value' => SLIDER_HOME,
            ),
        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
	
	public function blog_slider()
	{
	    $bc = new besc_crud();
	    $bc->table('slider');
	    $bc->primary_key('id');
	    $bc->title('Blog Slider');
	    $bc->where('slider_type = ' . SLIDER_BLOG);
	
	    $bc->custom_buttons(array());
	    $bc->list_columns(array(
	        'fname',
	    ));
	    
	    $bc->ordering(array(
	        'ordering' => 'ordering',
	        'value' => 'name',
	    ));
	
	
	    $bc->columns(array
	        (
	            'name' => array
	            (
	                'db_name' => 'name',
	                'type' => 'text',
	                'display_as' => 'Name',
	                'validation' => '',
	            ),
	            
	            'fname' => array(
	                'db_name' => 'fname',
	                'type' => 'image',
	                'display_as' => 'Blog image',
	                'col_info' => 'filetypes: .png, .jpg, .jpeg<br/>1000 x 405 px',
	                'accept' => '.png,.jpg,.jpeg',
	                'uploadpath' => 'items/uploads/slider'
	            ),
	
	            'continent_id' => array(
	                'db_name' => 'slider_type',
	                'type' => 'hidden',
	                'value' => SLIDER_BLOG,
	            ),
	        ));
	
	    $data['crud_data'] = $bc->execute();
	    $this->page('backend/crud', $data);
	}
}

