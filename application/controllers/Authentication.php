<?php

// error_reporting(E_NOTICE);
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Authentication extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Authentication_model', 'am');
    }

    public function showLogin()
    {
        $data = array();
        $this->load->view('authentication/login', $data);
    }

    public function checkLogin()
    {
        if ($this->logged_in()) 
        {
            $result = array(
                'success' => true,
                'message' => 'logged_id',
                'menu' => $this->lang->line('header_menu_logout')
            );
        } 
        else 
        {
            $result = array(
                'success' => false,
                'message' => 'not_logged_in',
                'menu' => $this->lang->line('header_menu_login')
            );
        }
        echo json_encode($result);
    }

    public function loginUser()
    {
        $username = purify($_POST['username']);
        $pword = purify($_POST['pword']);
        if ($this->am->getPW($username)->num_rows() > 0) 
        {
            $pwcheck = check_hash($pword, $this->am->getPW($username)->row()->pword);
            if ($pwcheck) 
            {
                $userId = $this->am->getUserdataByUsername($username)->row()->id;
                $this->session->set_userdata('user_id', $userId);
                redirect('backend');
            } 
            else 
            {
                $data['errormessage'] = "Password incorrect";
                $this->load->view('authentication/login', $data);
            }
        } 
        else 
        {
            $data['errormessage'] = "User not found";
            $this->load->view('authentication/login', $data);
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->sess_destroy();
        $this->load->view('authentication/logout', array());
    }
    
    

    
    public function press_login()
    {
        $email = $this->input->post('email');
        
        $pressuser = $this->am->getPressUserByEmail($email);
        
        if($pressuser->num_rows() == 1)
        {
            $this->session->set_userdata('tba21_press_logged_in', $pressuser->row()->id);
            $success = true;
            $message = "";
        }
        else
        {
            $success = false;
            $message = $this->lang->line('press_error_nologin');
        }
        
        echo json_encode(array(
            'success' => $success,
            'message' => $message,
        ));
    }
    
    public function press_registration()
    {
        if($this->language == LANG_DE)
            $this->config->set_item('language', 'german');
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('firstname', 'lang:press_reg_firstname', 'trim|required');
        $this->form_validation->set_rules('lastname', 'lang:press_reg_lastname', 'trim|required');
        $this->form_validation->set_rules('publication', 'lang:press_reg_publication', 'trim|required');
        $this->form_validation->set_rules('email', 'lang:press_reg_email', 'trim|required|valid_email|is_unique[pressuser.email]');
        
        if($this->form_validation->run() == FALSE)
        {
            $success = false;
            $this->form_validation->set_error_delimiters('', '');
            $message = nl2br($this->form_validation->error_string());
        }
        else
        {
            $this->am->insertPressUser(array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'publication' => $this->input->post('publication'),
                'email' => $this->input->post('email'),
            ));
            
            $success = true;
            $message = $this->lang->line('press_error_reg_success');
        }
    
        echo json_encode(array(
            'success' => $success,
            'message' => $message,
        ));
    }    
    
    public function press_logout()
    {
        $this->session->unset_userdata('tba21_press_logged_in');
        echo json_encode(array(
            'success' => true,
        ));
    }
    
    public function usersettings($success = false)
    {
        $this->load->helper('form');
        $data['success'] = $success;
        $data['user'] = $this->am->getUserdataByID($this->session->userdata('user_id'))->row();
        $this->load->view('authentication/settings', $data);
    }
    
    public function updateUser()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('firstname', 'First name', 'trim|max_length[50]');
        $this->form_validation->set_rules('lastname', 'Last name', 'trim|max_length[50]');
        $this->form_validation->set_rules('email', 'E-Mail', 'trim|max_length[255]|valid_email');
        $this->form_validation->set_rules('pword', 'New password', 'max_length[50]|min_length[8]');
        if($this->input->post('pword') != '' && $this->input->post('pword') != null)
            $this->form_validation->set_rules('pword2', 'Confirm password', 'max_length[50]|matches[pword]|required');
            
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('authentication/settings');
        }
        else
        {
            if($this->input->post('userid') == $this->session->userdata('user_id'))
            {
                $user = array(
                    'firstname' => $this->input->post('firstname'),
                    'lastname' => $this->input->post('lastname'),
                    'email' => $this->input->post('email'),
                );
                
                if($this->input->post('pword') != '' &&$this->input->post('pword') != null)
                {
                    require_once(APPPATH.'libraries/PasswordHash.php');
                    $hasher = new PasswordHash(8, false);
                    $user['pword'] = $hasher->HashPassword($this->input->post('pword'));
                }
                
                $this->am->updateUser($user, $this->session->userdata('user_id'));

                $this->usersettings(true);
            }
            else 
            {
                $this->logout();    
            }
        }
    }
}

/* End of file authentication.php */
/* Location: ./application/controllers/authentication.php */