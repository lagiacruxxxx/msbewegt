<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends MY_Controller 
{
	protected $user;
	protected $base_url;
	protected $module_tables = array();
	

    function __construct()
    {
        parent::__construct();
        
        date_default_timezone_set('Europe/Vienna');
        
		if(!$this->logged_in())
			redirect('authentication/showLogin');
		
		$this->load->model('Authentication_model');
		$this->user = $this->Authentication_model->getUserdataByID($this->session->userdata('user_id'))->row();
		$this->load->library('Besc_crud');
		$this->load->model('Backend_model', 'bm');
    }  

	public function index()
	{
		$this->page('backend/home', array());
	}

	/***********************************************************************************
	 * DISPLAY FUNCTIONS
	 **********************************************************************************/	
	
    public function page($content_view, $content_data = array())
    {
    	$data = array();
		$data['username'] = $this->user->username;
		$data['additional_css'] = isset($content_data['additional_css']) ? $content_data['additional_css'] : array();
		$data['additional_js'] = isset($content_data['additional_js']) ? $content_data['additional_js'] : array();
		
		$this->load->view('backend/head', $data);
		$this->load->view('backend/menu', $data);
        $this->load->view($content_view, $content_data);
		$this->load->view('backend/footer', $data);
    }	
    
    public function upload_image()
    {
        $this->load->helper('besc_helper');
         
        $filename = $_POST['filename'];
        $upload_path = $_POST['uploadpath'];
        if(substr($upload_path, -1) != '/')
            $upload_path .= '/';
         
        $rnd = rand_string(12);
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $serverFile = time() . "_" . $rnd . "." . $ext;
    
        $error = move_uploaded_file($_FILES['data']['tmp_name'], getcwd() . "/$upload_path$serverFile");
    
        echo json_encode(array('success' => true,
            'path' => getcwd() . "/$upload_path/$serverFile",
            'filename' => $serverFile));
    }
    
    public function uploadFile()
    {
        $filename = $this->input->post('filename');
        $upload_path = $this->input->post('uploadpath');
         
        $error = move_uploaded_file($_FILES['data']['tmp_name'], getcwd() . "/$upload_path/$filename");
         
        echo json_encode
        (
            array
            (
                'error' => $error,
                'success' => true,
                'filename' => $filename
            )
        );
    }
    
    
}
