<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller 
{
    
    function __construct()
    {
        parent::__construct();
    
        $this->load->model('Frontend_model', 'fm');
        $this->load->helper('besc_helper');
        $this->lang->load('frontend', 'german');
    }    
    
	public function index()
	{
	    $data = array();
	    $data['sliders'] = $this->fm->getSliders(SLIDER_HOME);
	    $data['mosaic'] = array();
	    $data['is_mobile'] = $this->is_mobile;
	    foreach($this->fm->getMosaic()->result() as $item)
	    {
	        switch($item->item_type)
	        {
	            case MOSAIC_TYPE_NEWS:
	                $data['mosaic'][] = (object)array(
                        'prettyurl' => $item->item_id != NEWS_OVERVIEW ? site_url('news/' . $this->fm->getNewsById($item->item_id)->row()->prettyurl) : site_url('news'),
                        'mosaic' => $item, 
	                );
	                break;
                case MOSAIC_TYPE_ARTICLE:
                case MOSAIC_TYPE_VIDEO:
                    $data['mosaic'][] = (object)array(
                        //'item' => $this->fm->getItemById($item->item_id)->row(),
                        'prettyurl' => $item->item_id != PARTNER_OVERVIEW ? site_url('artikel/' . $this->fm->getItemById($item->item_id)->row()->prettyurl) : site_url('vor-den-vorhang'),
                        'mosaic' => $item,
                    );
                    break;
                    	    
                case MOSAIC_TYPE_BLOG:
                    $data['mosaic'][] = (object)array(
                        'prettyurl' => $item->item_id != BLOG_OVERVIEW ? site_url('blog/' . $this->fm->getBlogEntryByID($item->item_id)->row()->prettyurl) : site_url('blogs'),
                        'mosaic' => $item,
                    );
                    break;
	        }
	    }
	    $this->_display('home', $data);	    
        
	}
	
	public function blogs()
	{
	    $data = array();
	    $data['bloggers'] = $this->fm->getBloggers();
	    $data['blogEntries'] = $this->fm->getBlogEntries();
	    $data['sliders'] = $this->fm->getSliders(SLIDER_BLOG);
	    $this->_display('blogs', $data);
	}
	
	public function blog_item($prettyurl)
	{
	    $item = $this->fm->getBlogEntryByPrettyUrl($prettyurl)->row();
	    $data = array();
	    $data = $this->_item($item, ITEM_TYPE_BLOG);
	    $this->_display('item', $data);
	}
	
	public function news_item($prettyurl)
	{
	    $item = $this->fm->getNewsByPrettyUrl($prettyurl)->row();
	    $data = array();
	    $data = $this->_item($item, ITEM_TYPE_NEWS);
	    $this->_display('item', $data);	    
	}
	
	public function news_list()
	{
	    $data['items'] = array();
	    foreach($this->fm->getNews()->result() as $item)
	    {
	        $data['items'][] = array(
                'item_type' => ITEM_TYPE_NEWS,
                'item_id' => $item->id,
	            'item' => $item,
                'date' => $item->created_date,  
	        );
	    }
	    $this->_display('itemlist', $data);
	}
	
	public function partners()
	{
	    $data['items'] = array();
	    foreach($this->fm->getPartners()->result() as $item)
	    {
	        $data['items'][] = array(
                'item_type' => ITEM_TYPE_PARTNER,
                'item_id' => $item->id,
	            'item' => $item,
	        );
	    }
	    $this->_display('itemlist', $data);
	}
	
	
	public function article_item($prettyurl)
	{
	    $item = $this->fm->getItemByPrettyUrl($prettyurl)->row();
	    $data = array();
	    $data = $this->_item($item, ITEM_TYPE_ARTICLE);
	    $this->_display('item', $data);
	}
	
	public function partner_item($prettyurl)
	{
	    $item = $this->fm->getPartnerByPrettyUrl($prettyurl)->row();
	    $data = array();
	    $data = $this->_item($item, ITEM_TYPE_PARTNER);
	    $this->_display('item', $data);	    
	}
	
	private function _item($item, $itemType)
	{
	    $data['item'] = $item;
	    
	    $data['module_text'] = $this->fm->getModuleTextByItemId($item->id, $itemType);
	    $data['module_image'] = $this->fm->getModuleImageByItemId($item->id, $itemType);
	    $data['module_video'] = $this->fm->getModuleVideoByItemId($item->id, $itemType);
	    $data['module_html'] = $this->fm->getModuleHTMLByItemId($item->id, $itemType);
	    $data['module_download'] = $this->fm->getModuleDownloadByItemId($item->id, $itemType);
	    	
	    $data['modules'] = array_merge(array(), $data['module_text']->result_array());
	    $data['modules'] = array_merge($data['modules'], $data['module_image']->result_array());
	    $data['modules'] = array_merge($data['modules'], $data['module_video']->result_array());
	    $data['modules'] = array_merge($data['modules'], $data['module_html']->result_array());
	    $data['modules'] = array_merge($data['modules'], $data['module_download']->result_array());
	    
	    usort($data['modules'], 'module_cmp');
	    
	    return $data;
	}
	
	
	private function _isCrawler()
	{
	    $ua = $_SERVER['HTTP_USER_AGENT'];
	    
	    if (in_array($_SERVER['HTTP_USER_AGENT'], array(
	        'facebookexternalhit/1.1 (+https://www.facebook.com/externalhit_uatext.php)',
	        'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)',
	        'facebookexternalhit/1.0 (+http://www.facebook.com/externalhit_uatext.php)',
	        'facebookexternalhit/1.0 (+https://www.facebook.com/externalhit_uatext.php)',
	        'facebookexternalhit/1.1',
	        'Facebot',
	    )))
	        return true;	        
	    else
	        return false;
	}
	
	
	private function _display($view, $viewdata)
	{
	    $data = array();
	    $data['is_mobile'] = $this->is_mobile;
	    $data['is_ipad'] = $this->is_ipad;
	    $data['menuitems'] = $this->_menuitems();
	    $data['settings'] = $this->fm->getSettings()->row();
	    $this->load->helper('cookie');
	    $data['size_css'] = get_cookie('msbewegt_size') != null ? get_cookie('msbewegt_size') : 'menusize_s';
	     
	    
	    $data = array_merge($data, $viewdata);
	    
	    $this->load->view('frontend/head', $data);
	    if(!$this->is_mobile)
            $this->load->view('frontend/menu', $data);
	    else
	        $this->load->view('frontend/menu_mobile', $data);
	         
	    $this->load->view('frontend/' . $view, $data);
	    $this->load->view('frontend/footer', $data);
	}
	
	private function _menuitems()
	{
	    $menuitems = $this->fm->getMenuitems()->result_array();
	    $fixeditems = array();
	    foreach($menuitems as $key => $value)
	    {
	        $fixeditems[$value['id']] = $value;
	        switch($value['item_id'])
	        {
	            case ITEM_ID_BLOGS:
	                $fixeditems[$value['id']]['prettyurl'] = site_url('blogs');
	                break;
                case ITEM_ID_NEWS:
                    $fixeditems[$value['id']]['prettyurl'] = site_url('news');
                    break;
                case ITEM_ID_PARTNERS:
                    $fixeditems[$value['id']]['prettyurl'] = site_url('vor-den-vorhang');
                    break;    
	            default:   
	                $fixeditems[$value['id']]['prettyurl'] = site_url('artikel/' . $this->fm->getItemById($value['item_id'])->row()->prettyurl);
	                break;
	        }
	        
	    }
	    return $fixeditems;
	}
	
	
	public function search()
	{
	    $input = $this->input->get('input');
	    
	    $results = array();
	    
	    if(strpos(strtolower($input), 'blog') !== false)
	    {
	        $results[] = array(
	            'item_id' => -999,
                'item_type' => ITEM_TYPE_STATICBLOG,
            ); 
	    }
	    	    
	    // search news
	    foreach($this->fm->search_news($input)->result() as $item)
	    {
	        $results[] = array(
                'item_type' => ITEM_TYPE_NEWS,
                'item_id' => $item->id,
                'item' => $item,
                'date' => $item->created_date,  
            ); 
	    }
	    
	    // search articles
	    foreach($this->fm->search_items($input)->result() as $item)
	    {
	        $results[] = array(
	            'item_type' => ITEM_TYPE_ARTICLE,
	            'item_id' => $item->id,
	            'item' => $item,
	            'date' => $item->created_date,
	            'teaser' => besc_trim($this->fm->getFirstTextModule($item->id, ITEM_TYPE_ARTICLE)->row()->content, 150),
	        );
	    }
	    
	    // blog articles
	    foreach($this->fm->search_blogs($input)->result() as $item)
	    {
	        $results[] = array(
	            'item_type' => ITEM_TYPE_BLOG,
	            'item_id' => $item->id,
	            'item' => $item,
	            'date' => $item->created_date,
	            'img' => $this->fm->getBloggerById($item->blogger_id)->row()->img_active,
	        );
	    }
	    
	    // text modules
	    foreach($this->fm->getTextModules($input)->result() as $module)
	    {
	        $exists = false;
	        foreach($results as $item)
	        {
	            if($item['item_id'] == $module->item_id && $item['item_type'] == $module->item_type && $item['item_type'])
	                $exists = true;
	        }
	        if(!$exists)
	        {
    	        switch($module->item_type)
    	        {
    	            case ITEM_TYPE_ARTICLE:
    	                $item = $this->fm->getItemById($module->item_id);
    	                if($item->num_rows() > 0)
    	                {
    	                    $item = $item->row();
    	                     
        	                $results[] = array(
        	                    'item_type' => ITEM_TYPE_ARTICLE,
        	                    'item_id' => $item->id,
        	                    'item' => $item,
        	                    'date' => $item->created_date,
        	                    'teaser' => besc_trim($this->fm->getFirstTextModule($item->id, ITEM_TYPE_ARTICLE)->row()->content, 150),
        	                );
    	                }
    	                break;
    	                
    	            case ITEM_TYPE_BLOG:
    	                $item = $this->fm->getBlogEntryByID($module->item_id);
    	                if($item->num_rows() > 0)
    	                {
    	                    $item = $item->row();
    	                     
        	                $results[] = array(
        	                    'item_type' => ITEM_TYPE_BLOG,
        	                    'item_id' => $item->id,
        	                    'item' => $item,
        	                    'date' => $item->created_date,
        	                    'img' => $this->fm->getBloggerById($item->blogger_id)->row()->img_active,
        	                );
    	                }
                        break;
    	                
    	            case ITEM_TYPE_NEWS:
    	                $item = $this->fm->getNewsById($module->id);
    	                if($item->num_rows() > 0)
    	                {
                            $item = $item->row();
    	                
        	                $results[] = array(
        	                    'item_type' => ITEM_TYPE_NEWS,
        	                    'item_id' => $item->id,
        	                    'item' => $item,
        	                    'date' => $item->created_date,
        	                );
    	                }   
    	                break;
    	        }
	        }
	    }
	     
	    $data['items'] = $results;
	    $this->_display('itemlist', $data);
	}
}

