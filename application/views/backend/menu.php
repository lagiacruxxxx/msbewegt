
<div id="messagecontainer"></div>

<div id="menu">
	<ul>
		<li><a href="<?= site_url('authentication/logout')?>">Logout</a></li>
		<li><a href="<?= site_url('authentication/usersettings')?>"><img src="<?= site_url('items/backend/img/settings.png')?>" /></a></li>
		<li>Logged in as <b><?= $username?></b></li>
	</ul>
</div>

<div id="sidebar">
    <div class="sidebar_logo">
        <img src="<?= site_url('items/backend/img/logo.png')?>" />
    </div>

	<ul>
		<li class="sidebar_headline"><a href="<?= site_url('entities/Menu/mainmenu')?>">MENU</a></li>
		<li class="sidebar_headline"><a href="<?= site_url('entities/Blog/bloggers')?>">BLOGGER</a></li>
		<li class="sidebar_headline"><a href="<?= site_url('entities/Mosaic/items')?>">MOSAIC</a></li>
		<li class="separator"></li>
		<li class="sidebar_headline"><a href="<?= site_url('entities/Item/items')?>">ARTICLES</a></li>
		<li class="sidebar_headline"><a href="<?= site_url('entities/Blog/entries')?>">BLOG ENTRIES</a></li>
		<li class="sidebar_headline"><a href="<?= site_url('entities/News/articles')?>">NEWS ARTICLES</a></li>
		<li class="sidebar_headline"><a href="<?= site_url('entities/Partner/partners')?>">PARTNERS</a></li>
		<li class="separator"></li>
		<li class="sidebar_headline"><a href="<?= site_url('entities/Settings/home_slider')?>">HOME SLIDER</a></li>
		<li class="sidebar_headline"><a href="<?= site_url('entities/Settings/blog_slider')?>">BLOG SLIDER</a></li>
		
	</ul>
</div>

