

        <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/imgareaselect-default.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/tetrahaedron.css"); ?>">

        <script type="text/javascript" src="<?=site_url("items/backend/js/jquery.imgareaselect.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/backend/js/tetrahaedron.js"); ?>"></script>
        
        
        <script>var itemId = <?= $item->id?>;</script>

        <div id="content">
        
            <div class="content_h1">Tetrahaedron image conversion</div>
        
            <div id="current_images">
                <div class="content_h4">Current images</div>
                <div id="current_image_container">
                    <img src="<?= site_url('items/uploads/items/' . $item->image)?>" class="original" title="Current image" data-text="Current image"/>
                    <img src="<?= site_url('items/uploads/items/' . $item->image_blurred)?>" class="blurred" title="Current image blurred" data-text="Current image blurred"/>
                </div>
                
                <div id="mirrored_image_container">
                    <img src="<?= site_url('items/uploads/items/' . $item->mirror_image)?>" class="mirrored" title="Current image" data-text="Current image"/>
                    <img src="<?= site_url('items/uploads/items/' . $item->mirror_image_blurred)?>" class="mirrored_blurred" title="Current image blurred" data-text="Current image blurred"/>
                </div>
            </div>
            
            <div style="clear: both;"></div>
        
            <div class="content_h4">Create new images</div>
            <div id="crop_container">
                <div id="crop_image">
                    <img class="crop" src="<?= site_url('items/uploads/detailimg/' .($fname != "" ? $fname : 'image_upload_placeholder.png'))?>" filename="<?= ($fname != "" ? $fname : 'image_upload_placeholder.png')?>">
                    <input type="file" id="upload_crop_input" /> 
                </div>
                <div id="crop_buttons">
                    <div class="crop_button upload_crop_button">Select image</div>
                    <div class="crop_button crop_and_save">Crop and save</div>
                    <br>
                    <div class="crop_button mask" mask=0>Mask 1</div>
                    <div class="crop_button mask" mask=1>Mask 2</div>
                    <div class="crop_button mask" mask=2>Mask 3</div>
                    <div class="crop_button mask" mask="random">Mask random</div>
                </div>
            </div>
            
        
        </div>
        
        