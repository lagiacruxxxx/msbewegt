<html>
    <head>
    	<?php if($is_mobile):?>
    		<meta name="viewport" content="width=320, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no">
        <?php elseif($is_ipad):?>
        	<meta name="viewport" content="width=1280, minimum-scale=0.5, maximum-scale=2.0, user-scalable=no">
        <?php endif;?>
        <meta charset="UTF-8">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <meta property="og:title" content="MS bewegt"/>
    	<meta property="og:type" content="website"/>
    	<meta property="og:url" content="http://www.ms-bewegt.at/"/>
    	<meta property="og:image" content="<?= site_url('items/frontend/img/fb_share.jpg')?>"/>
    	<meta property="og:description" content="" />
    
        <title>MS Bewegt</title>
    
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/reset.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/desktop.css"); ?>">
        <link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/item.css"); ?>">
        
        <?php if($is_mobile):?>
    		<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/mobile.css"); ?>">
        <?php else:?>
    		<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/$size_css.css"); ?>">
    	<?php endif;?>		
        
        <script type="text/javascript" src="<?=site_url("items/general/js/jquery-1.11.2.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/isotope.pkgd.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/packery.pkgd.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/placeholders.min.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/cookie.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/responsive.js"); ?>"></script>
        <script type="text/javascript" src="<?=site_url("items/frontend/js/desktop.js"); ?>"></script>
        <?php if($is_mobile):?>
    		<script type="text/javascript" src="<?=site_url("items/frontend/js/mobile.js"); ?>"></script>
    	<?php endif;?>		
        
        <script>
            var rootUrl = "<?= site_url()?>";
            var is_mobile = <?= $is_mobile ? 'true' : 'false'?>;
            var is_ipad = <?= $is_ipad ? 'true' : 'false'?>;
        </script>
        
    <!-- GA -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-5934189-72', 'auto');
      ga('send', 'pageview');
    
    </script>
    	
    </head>

    <body>
        <div id="menu_bg"></div>
        <div id="container">