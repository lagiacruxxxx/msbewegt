

        <div id="home" class="contentcontainer">
            <div id="slider_container">
                <?php $i = 0; foreach($sliders->result() as $slider_item):?>
                    <img class="slider_item" status="<?php if($i == 0):?>active<?php else:?>inactive<?php endif;?>" i=<?= $i++?> src="<?= site_url('items/uploads/slider/' . $slider_item->fname)?>"  />
                <?php endforeach;?>
            </div>
            
            <div id="mosaic">
                <?php foreach($mosaic as $item): ?>
                    <?php if($item->mosaic->item_type == MOSAIC_TYPE_NEWS):?>
                        <div class="mosaic_item small" >
                            <a href="<?= $item->prettyurl?>"><img class="mosaic_small_img" src="<?= site_url('items/uploads/mosaic/' . $item->mosaic->teaser_image)?>" ></a>
                            <div class="mosaic_small_text">
                                <div class="mosaic_small_text_header"><?= htmlspecialchars($item->mosaic->name)?></div>
                                <div class="mosaic_small_text_content"><?= htmlspecialchars($item->mosaic->teaser_text)?></div>
                            </div>
                            <div class="mosaic_small_readmore">
                                <a href="<?= $item->prettyurl?>"><span><?= $this->lang->line('readmore')?></span></a>
                            </div>
                        </div>
                    <?php endif;?>
                    
                    <?php if($item->mosaic->item_type == MOSAIC_TYPE_ARTICLE):?>
                        <div class="mosaic_item small" >
                            <a href="<?= $item->prettyurl?>"><img class="mosaic_small_img" src="<?= site_url('items/uploads/mosaic/' . $item->mosaic->teaser_image)?>" ></a>
                            <div class="mosaic_small_text">
                                <div class="mosaic_small_text_header"><?= htmlspecialchars($item->mosaic->name)?></div>
                                <div class="mosaic_small_text_content"><?= htmlspecialchars($item->mosaic->teaser_text)?></div>
                            </div>
                            <div class="mosaic_small_readmore">
                                <a href="<?= $item->prettyurl?>"><span><?= $this->lang->line('readmore')?></span></a>
                            </div>
                        </div>
                    <?php endif;?>
                    
                    <?php if($item->mosaic->item_type == MOSAIC_TYPE_VIDEO):?>
                        <?php if(!$is_mobile):?>
                        <div class="mosaic_item big" >
                            <a href="<?= $item->prettyurl?>"><img class="mosaic_big_img" src="<?= site_url('items/uploads/mosaic/' . $item->mosaic->teaser_image)?>" ></a>
                            <div class="mosaic_big_text" style="color: <?= $item->mosaic->textcolor?>;">
                                <div class="mosaic_big_text_dummy">
                                    <div class="mosaic_big_text_content"><?= $item->mosaic->teaser_text?></div>
                                    <div class="mosaic_big_readmore">
                                        <a href="<?= $item->prettyurl ?>"><span style="border-color: <?= $item->mosaic->textcolor?>;"><?= $this->lang->line('readmore')?></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php else:?>
                        <div class="mosaic_item small" >
                            
                            <div class="mosaic_small_text">
                                <div class="mosaic_small_text_header"><?= $item->mosaic->teaser_text?></div>
                            </div>
                            <div class="mosaic_small_readmore">
                                <a href="<?= site_url('artikel/' . $item->item->prettyurl)?>"><span style="border-color: <?= $item->mosaic->textcolor?>;"><?= $this->lang->line('readmore')?></span></a>
                            </div>
                        </div>
                        <?php endif;?>
                    <?php endif;?>
                    
                    <?php if($item->mosaic->item_type == MOSAIC_TYPE_BLOG):?>
                        <?php if(!$is_mobile):?>
                        <div class="mosaic_item big" >
                            <a href="<?= $item->prettyurl?>"><img class="mosaic_big_img" src="<?= site_url('items/uploads/mosaic/' . $item->mosaic->teaser_image)?>" ></a>
                            <div class="mosaic_blog_text">
                                <div class="mosaic_blog_text_content1"><?= $item->mosaic->teaser_text?></div>
                                <div class="mosaic_blog_text_separator"></div>
                                <div class="mosaic_blog_text_content2"><?= $item->mosaic->teaser_text2?></div>
                            </div>
                            <div class="mosaic_blog_readmore">
                                <a href="<?= $item->prettyurl?>"><span><?= $this->lang->line('readmore')?></span></a>
                            </div>
                        </div>
                        <?php else:?>
                        <div class="mosaic_item small" >
                            
                            <div class="mosaic_small_text">
                                <div class="mosaic_small_text_header"><?= $item->mosaic->teaser_text?></div>
                                <div class="mosaic_small_text_content"><?= $item->mosaic->teaser_text2?></div>
                            </div>
                            <div class="mosaic_small_readmore">
                                <a href="<?= $item->prettyurl?>"><span><?= $this->lang->line('readmore')?></span></a>
                            </div>
                        </div>
                        <?php endif;?>
                    <?php endif;?>
                    
                <?php endforeach;?>
            </div>
        </div>
        