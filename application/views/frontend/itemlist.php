

        <div id="itemlist" class="contentcontainer">
            <?php $i = 0; foreach($items as $item):?>
                <?php if($item['item_type'] ==  ITEM_TYPE_NEWS):?>
                <div class="itemlist_item">
                    <a href="<?= site_url('news/' . $item['item']->prettyurl)?>" >
                        <div class="itemlist_text">
                            <div class="itemlist_text_header"><?= $item['item']->name?></div>
                            <div><?= $item['item']->teaser_text?></div>
                            <div class="news_readmore"><?= $this->lang->line('news_readmore')?></div>
                        </div>
                        <?php if($item['item']->teaser_image != ''):?>
                            <div class="itemlist_img"><img src="<?= site_url('items/uploads/news/' . $item['item']->teaser_image)?>" /></div>
                        <?php endif;?>
                    </a>
                </div>
                <?php endif;?>
                
                <?php if($item['item_type'] ==  ITEM_TYPE_ARTICLE):?>
                <div class="itemlist_item">
                    <a href="<?= site_url('artikel/' . $item['item']->prettyurl)?>" >
                        <div class="itemlist_text">
                            <div class="itemlist_text_header"><?= $item['item']->header?></div>
                            <div><?= $item['teaser']?></div>
                        </div>
                        <?php if($item['item']->detail_img != '' && $item['item']->detail_img != 'image_upload_placeholder.png'):?>
                            
                        <div class="itemlist_img">
                            <img src="<?= site_url('items/uploads/detailimg/' . $item['item']->detail_img)?>" />
                        </div>
                        <?php endif;?>
                    </a>
                </div>
                <?php endif;?>
                
                <?php if($item['item_type'] ==  ITEM_TYPE_BLOG):?>
                <div class="itemlist_item">
                    <a href="<?= site_url('blog/' . $item['item']->prettyurl)?>" >
                        <div class="itemlist_text">
                            <div class="itemlist_text_header"><?= $item['item']->header?></div>
                            <div><?= $item['item']->teaser_text?></div>
                        </div>
                        <div class="itemlist_img"><img src="<?= site_url('items/uploads/bloggers/' . $item['img'])?>" /></div>
                    </a>
                </div>
                <?php endif;?>
                
                <?php if($item['item_type'] ==  ITEM_TYPE_PARTNER):?>
                <div class="itemlist_item">
                    <a href="<?= site_url('partner/' . $item['item']->prettyurl)?>" >
                        <div class="itemlist_text">
                            <div class="itemlist_text_header"><?= $item['item']->name?></div>
                            <div><?= $item['item']->teaser_text?></div>
                            <div class="news_readmore"><?= $this->lang->line('news_readmore')?></div>
                        </div>
                        <?php if($item['item']->teaser_image != ''):?>
                            <div class="itemlist_img"><img src="<?= site_url('items/uploads/news/' . $item['item']->teaser_image)?>" /></div>
                        <?php endif;?>
                    </a>
                </div>
                <?php endif;?>
                
                <?php if($item['item_type'] ==  ITEM_TYPE_STATICBLOG):?>
                <div class="itemlist_item">
                    <a href="<?= site_url('blogs')?>" >
                        <div class="itemlist_text">
                            <div class="itemlist_text_header">365 Tage mit MS</div>
                            <div>Hier finden sie die Auflistung unserer Blogger.</div>
                            <div class="news_readmore"><?= $this->lang->line('news_readmore')?></div>
                        </div>
                        <div class="itemlist_img"><img src="<?= site_url('items/frontend/img/mobile_blog.png')?>" /></div>
                    </a>
                </div>
                <?php endif;?>
            <?php $i++; endforeach;?>
                
            <?php if($i <= 0):?>
                <div id="noresults">Leider wurden keine Treffer gefunden.</div>
            <?php endif;?>
        </div>