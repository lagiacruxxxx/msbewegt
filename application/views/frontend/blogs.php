

        <div id="blogs" class="contentcontainer">
            <div id="slider_container">
                <?php $i = 0; foreach($sliders->result() as $slider_item):?>
                    <img class="slider_item" status="<?php if($i == 0):?>active<?php else:?>inactive<?php endif;?>" i=<?= $i++?> src="<?= site_url('items/uploads/slider/' . $slider_item->fname)?>"  />
                <?php endforeach;?>
            </div>
            
            <div id="blog_select">
                <?php foreach($bloggers->result() as $blogger):?>
                    <div class="blogger" blogger="<?= $blogger->name?>">
                        <img class="blogger_active" src="<?= site_url('items/uploads/bloggers/' . $blogger->img_active)?>" />
                        <img class="blogger_inactive" src="<?= site_url('items/uploads/bloggers/' . $blogger->img_inactive)?>" />
                        <div class="blogger_desc"><?= nl2br($blogger->desc)?></div>
                    </div>
                <?php endforeach;?>
            </div>
            
            <div id="blog_info">
                Klicken Sie auf die obigen Portraits der BloggerInnen um die Beiträge zu filtern.
            </div>
            
            <div id="blog_entries">
                <?php foreach($blogEntries->result() as $entry):?>
                    <a href="<?= site_url('blog/' . $entry->prettyurl) ?>">
                        <div class="blog_entry <?= $entry->bloggername?>">
                            <img class="blog_header_img" src="<?= site_url('items/uploads/bloggers/' . $entry->entry_header)?>" />
                            <div class="blog_header_text"><b><?= strtoupper($entry->bloggername)?></b><br/><?= date('d.m.Y', strtotime($entry->created_date))?></div>
                            <div class="blog_headline"><?= $entry->name?></div>
                            <div class="blog_teaser"><?= $entry->teaser_text?></div>
                        </div>
                    </a>
                <?php endforeach;?>
            </div>
        </div>