    
        <div id="menu">

            <div class="menubutton noselect" ><a href="<?= $menuitems[MENU_NEWS]['prettyurl']?>"><?= $menuitems[MENU_NEWS]['name']?></a></div>
            <div class="menubutton noselect about_submenu">ÜBER</div>
            <div class="menubutton noselect "><a href="<?= $menuitems[MENU_BLOGS]['prettyurl']?>"><?= $menuitems[MENU_BLOGS]['name']?></a></div>
            <div class="menubutton noselect"><a href="<?= $menuitems[MENU_EXPERTS]['prettyurl']?>"><?= $menuitems[MENU_EXPERTS]['name']?></a></div>
            <div class="menubutton noselect"><a href="<?= $menuitems[MENU_WORKING_WITH_MS]['prettyurl']?>"><?= $menuitems[MENU_WORKING_WITH_MS]['name']?></a></div>
            <div class="menubutton noselect" style="margin-right: 0px;"><a href="<?= $menuitems[MENU_BEHINDTHECURTAIN]['prettyurl']?>"><?= $menuitems[MENU_BEHINDTHECURTAIN]['name']?></a></div>
            
            <div id="menusize">
                <div class="menusize_select" id="menusize_s">A</div>
                <div class="menusize_select" id="menusize_m">A</div>
                <div class="menusize_select" id="menusize_l">A</div>
            </div>
            
            <div id="menusearch">
                <img id="search_toggle" src="<?= site_url('items/frontend/img/icon_search.png') ?>" />
            </div>
            
        </div>
        <a href="<?= site_url()?>"><div id="logo"></div></a>
        <div id="menulist" class="menuhidden">
            <div class="menulistitem"><a href="<?= $menuitems[MENU_ABOUT_FOUNDATION]['prettyurl']?>"><?= $menuitems[MENU_ABOUT_FOUNDATION]['name'] ?></a></div>
            <div class="menulistitem"><a href="<?= $menuitems[MENU_ABOUT_INITIATIVE]['prettyurl']?>"><?= $menuitems[MENU_ABOUT_INITIATIVE]['name'] ?></a></div>            
            <div class="menulistitem"><a href="<?= $menuitems[MENU_ABOUT_MS]['prettyurl']?>"><?= $menuitems[MENU_ABOUT_MS]['name'] ?></a></div>        
        </div>
        
        <div id="searchbar" class="menuhidden">
            <input type="text" id="menusearch_input" value="" placeholder="<?= $this->lang->line('menu_search_placeholder')?>">
        </div>
        
        