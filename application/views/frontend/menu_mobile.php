    
        <div id="menu">
            <div id="mobile_menu"><?= $this->lang->line('mobile_menu')?></div> 
        </div>
        <a href="<?= site_url()?>"><div id="logo"></div></a>
        
        <div id="menu_mobile" class="hidden">
            <div id="menu_mobile_close"></div>
        
            <div class="menubutton noselect" ><a href="<?= $menuitems[MENU_NEWS]['prettyurl']?>"><?= $menuitems[MENU_NEWS]['name']?></a></div>
            <div class="menubutton noselect">ÜBER</div>
            
            <div class="menulistitem"><a href="<?= $menuitems[MENU_ABOUT_FOUNDATION]['prettyurl']?>"><?= $menuitems[MENU_ABOUT_FOUNDATION]['name'] ?></a></div>
            <div class="menulistitem"><a href="<?= $menuitems[MENU_ABOUT_INITIATIVE]['prettyurl']?>"><?= $menuitems[MENU_ABOUT_INITIATIVE]['name'] ?></a></div>            
            <div class="menulistitem"><a href="<?= $menuitems[MENU_ABOUT_MS]['prettyurl']?>"><?= $menuitems[MENU_ABOUT_MS]['name'] ?></a></div>
            
            <div class="menubutton noselect "><a href="<?= $menuitems[MENU_BLOGS]['prettyurl']?>"><?= $menuitems[MENU_BLOGS]['name']?></a></div>
            <div class="menubutton noselect"><a href="<?= $menuitems[MENU_EXPERTS]['prettyurl']?>"><?= $menuitems[MENU_EXPERTS]['name']?></a></div>
            <div class="menubutton noselect"><a href="<?= $menuitems[MENU_WORKING_WITH_MS]['prettyurl']?>"><?= $menuitems[MENU_WORKING_WITH_MS]['name']?></a></div>
            <div class="menubutton noselect"><a href="<?= $menuitems[MENU_BEHINDTHECURTAIN]['prettyurl']?>"><?= $menuitems[MENU_BEHINDTHECURTAIN]['name']?></a></div>
            
            <div id="menu_search">
                <img src="<?= site_url('items/frontend/img/icon_search.png') ?>" />
                <input type="text" id="mobile_search" />
            </div>
        </div>
        