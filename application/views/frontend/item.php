
        
        <div id="item" class="contentcontainer">
        
            <?php if($item->detail_type == DETAIL_TYPE_IMAGE):?>
                <?php if($item->detail_img != 'image_upload_placeholder.png'):?>
                    <div id="itemheader_img">
                        <img src="<?= site_url('items/uploads/detailimg/' . $item->detail_img)?>" />
                    </div>
                <?php else:?>
                    <div id="no_header"></div>
                <?php endif;?>
            <?php else:?>
                <?= $is_mobile ? $item->detail_html_mobile : $item->detail_html?>        
            <?php endif;?>  
            
            <?php if($item->detail_img != ''):?>
            
            <?php endif;?>
            
            <div id="itemcontainer">
                <div id="itemheader"><?= $item->header?></div>
                <div id="itemcol_big">
                    <?php foreach($modules as $module):?>
                    <?php if($module['column_id'] == 0):?>
                        <?php 
                            switch($module['mod'])
                            {
                                case 'text':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_text" style="">' . $module['content'] . '</div>';
                                    break;
                                case 'image':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_image" style=""><img src="' . site_url('items/uploads/module_image/' . $module['fname']) . '" /></div>';
                                    break;
                                case 'bulletpoint':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_bulletpoint" style=""><div class="enum_head"><span>' . $module['header'] . '</span></div><div class="enum_body">' . $module['content'] . '</div></div>';
                                    break;
                                case 'video':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_video" style=""><iframe class="module_video_iframe" src="https://www.youtube.com/embed/' . $module['code'] . '?start=' . $module['start'] . '&amp;wmode=transparent" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe></div>';
                                    break;
                                case 'html':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_html" style="">' . $module['html'] . '</div>';
                                    break;
                                case 'headline':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_headline" style=""><span>' . $module['headline'] . '</span></div>';
                                    break;
                                case 'download':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_download" style=""><a href="'. site_url('items/uploads/module_download/' . $module['fname']) . '" target="_blank"><div class="module_download_field">' . $module['description'] . '</div></a></div>';
                                    break;
                                    
                            }
                        ?>
                    <?php endif;?>
                <?php endforeach;?>
                </div>
                
                <div id="itemcol_small">
                    <?php foreach($modules as $module):?>
                    <?php if($module['column_id'] == 1):?>
                        <?php 
                            switch($module['mod'])
                            {
                                case 'text':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_text" style="">' . $module['content'] . '</div>';
                                    break;
                                case 'image':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_image" style=""><img src="' . site_url('items/uploads/module_image/' . $module['fname']) . '" /></div>';
                                    break;
                                case 'bulletpoint':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_bulletpoint" style=""><div class="enum_head"><span>' . $module['header'] . '</span></div><div class="enum_body">' . $module['content'] . '</div></div>';
                                    break;
                                case 'video':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_video" style=""><iframe class="module_video_iframe" src="https://www.youtube.com/embed/' . $module['code'] . '?start=' . $module['start'] . '&amp;wmode=transparent" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe></div>';
                                    break;
                                case 'html':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_html" style="">' . $module['html'] . '</div>';
                                    break;
                                case 'headline':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_headline" style=""><span>' . $module['headline'] . '</span></div>';
                                    break;
                                case 'download':
                                    echo '<div module_id=' . $module['id'] . ' class="module module_download" style=""><a href="'. site_url('items/uploads/module_download/' . $module['fname']) . '" target="_blank"><div class="module_download_field">' . $module['description'] . '</div></a></div>';
                                    break;
                            }
                        ?>
                    <?php endif;?>
                <?php endforeach;?>
                </div>
            </div>
        </div>