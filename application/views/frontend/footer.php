
            <div id="footer">
                <div id="footer_line"></div>
                <div id="footer_elements">
                    <div class="footer_element"><a href="<?= $menuitems[MENU_CONTACT]['prettyurl']?>"><?= $menuitems[MENU_CONTACT]['name'] ?></a></div>
                    <div class="footer_element"><a href="<?= $menuitems[MENU_IMPRESS]['prettyurl']?>"><?= $menuitems[MENU_IMPRESS]['name'] ?></a></div>
                    <div class="footer_element"><a href="<?= $menuitems[MENU_DATAPROTECTION]['prettyurl']?>"><?= $menuitems[MENU_DATAPROTECTION]['name'] ?></a></div>
                    <div class="footer_element"><?= $this->lang->line('footer_copyright')?></div>
                </div>
                <div id="footer_text"><?= $this->lang->line('footer_text')?></div>
            </div>
        </div>
    </body>
</html>