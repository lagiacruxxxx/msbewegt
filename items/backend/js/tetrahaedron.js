var areaselect = null;
var img_data = null;

$(document).ready(function()
{	
	igniteCrop();
	toggleCropButtonListeners(true);
});


function igniteCrop()
{

	areaselect = $('.crop').imgAreaSelect(
	{ 
		aspectRatio: '298:344', 
		handles: true,
		x1: 0,
		y1: 0,
		x2: 298,
		y2: 344,
		parent: '#crop_image',
		instance: true,
	});
}


function toggleCropButtonListeners(toggle)
{
	
	if(toggle)
	{
		$('.crop_and_save').click(function()
		{
			var selection = areaselect.getSelection();
			var element = $(this).attr('id');
			var url;
			var file = document.getElementById('upload_crop_input').files[0];
			var reader = new FileReader();
			
			reader.readAsDataURL (file);
			reader.onload = function(event)
			{
				var result = event.target.result;
				$.ajax(
				{
					url: 'http://54.217.247.147/tba21/tba.php',
					data: { filename: $('#crop_image img').attr('filename'), data: result , x1: selection.x1, y1: selection.y1, x2: selection.x2, y2: selection.y2, mask: $('.imgareaselect-selection').attr('mask')},
					method: 'POST',
					cache: false,
					dataType: 'json',
					success: function(data)
					{
						var ret = data;
						
						if(ret.success)
						{
							$.ajax(
							{
								url: rootUrl + 'entities/Item/saveBlurMask',
								data: {filename: ret.filename, itemId: itemId, mask: $('.imgareaselect-selection').attr('mask')},
								method: 'POST',
								dataType: 'json',
								success: function(data)
								{
									$('.original').attr('src', rootUrl + 'items/uploads/items/' + ret.filename);
									$('.blurred').attr('src', rootUrl + 'items/uploads/items/blur_' + ret.filename);
									$('.mirrored').attr('src', rootUrl + 'items/uploads/items/mirror_' + ret.filename);
									$('.mirrored_blurred').attr('src', rootUrl + 'items/uploads/items/blur_mirror_' + ret.filename);
								}
							});
						}
						else
						{
							alert('Error while cropping/masking/blurring');
						}
					}
				});					
			};
		});
		
		$('.upload_crop_button').click(function()
		{
			$('#upload_crop_input').click();
		});
		
		$('#upload_crop_input').change(function()
		{
			/*var element = $(this).attr('id');
			var file = document.getElementById(element).files[0];
			var uploadpath = 'items/uploads/crop';
			var reader = new FileReader();
			var url;
			
			reader.readAsDataURL (file);
			reader.onload = function(event)
			{
				var result = event.target.result;
				img_data = result;
				$.ajax(
				{
					url: rootUrl + 'entities/Item/upload_image',
					data: { filename: file.name, data: result , uploadpath: uploadpath},
					method: 'POST',
					success: function(data)
					{
						var ret = $.parseJSON(data);
						
						if(ret.success)
						{
							$('#crop_image img').attr('src', rootUrl + 'items/uploads/crop/' + ret.filename);
							$('#crop_image img').attr('filename', ret.filename);
						}
						else
						{
							alert('Error while uploading');
						}
					}
				});			
			};*/
			
			var uploadpath = 'items/uploads/crop';
			var xhr = new XMLHttpRequest();		
			var fd = new FormData;
			var files = this.files;
			
			fd.append('data', files[0]);
			fd.append('filename', files[0].name);
			fd.append('uploadpath', uploadpath);
			
			xhr.addEventListener('load', function(e) 
			{
				var ret = $.parseJSON(this.responseText);
				
				if(ret.success)
				{
					$('#crop_image img').attr('src', rootUrl + 'items/uploads/crop/' + ret.filename);
					$('#crop_image img').attr('filename', ret.filename);
				}
				else
				{
					alert('Error while uploading');
				}
		    });
			
			xhr.open('post', rootUrl + 'entities/Item/upload_image');
			xhr.send(fd);
		});
		
		$('.mask').click(function()
		{
			var mask = $(this).attr('mask');
			if(mask == 'random')
				mask = Math.floor(Math.random() * (3));
			
			$('.imgareaselect-selection').removeClass('mask_0');
			$('.imgareaselect-selection').removeClass('mask_1');
			$('.imgareaselect-selection').removeClass('mask_2');
			$('.imgareaselect-selection').addClass('mask_' + mask);
			$('.imgareaselect-selection').attr('mask', mask);
		});
	}
	else
	{
		$('.crop_and_save').unbind('click');
		$('.upload_crop_button').unbind('click');
		$('#upload_crop_input').unbind('change');
		$('.mask').unbind('click');
	}
	
}