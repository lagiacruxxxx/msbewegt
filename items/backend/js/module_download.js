

function new_module_download(id, parent)
{
	var new_elem = 
	{
		module: null,
		type: 'download',
		id: id,
		parent: parent,
		fname: '',
		uploadpath: 'items/uploads/module_image',
		description: '',
		
		
		// MODULE BASIC
		init: function(parent)
		{
			this.module = $('.module_backend[module_id=' + this.id + ']');
			this.parent = this.module.parent();
			this.description = this.module.find('.module_download_field').text();
			this.fname = this.module.find('.module_download_field').attr('fname');
			this.bindListeners();
			igniteDragging();
		},
		
		getPrototypeHTML: function()
		{
			var html = '<div module_id=' + this.id + ' class="module_backend module_download has_placeholder" data-text="Upload file here" style="top: ' + (Math.ceil(resize_col(this.parent)/10) * 10) + 'px;"></div>';
			return html;
		},
		
		bindListeners: function()
		{
			this.module.dblclick(function()
			{
				active_module = $(this);
				
				$('#popup_module_download').find('.popup_cancel_button').unbind('click');
				$('#popup_module_download').find('.popup_save_button').unbind('click');
				$('#popup_module_download').find('.popup_delete_button').unbind('click');
				
				$('#popup_module_download').find('#file_display_name').val(active_module.find('.module_download_field').text());
				
				$('#popup_module_download').show();
				
				$('#popup_module_download').find('.popup_save_button').click(function()
				{
					var text = $('#file_display_name').val();
					var fname =  $('#file_upload_input').val();
					
					var html = '<div class="module_download_field">' + text + '</div>';
					modules[active_module.attr('module_id')].setDescription(text);
					
					active_module.empty();
					active_module.append(html);
					active_module.attr('text', text);
					resize_col(active_module.parent());
					$('#popup_module_download').hide();
					$('#file_display_name').val('');
					$('#file_upload_input').val('');
				});
				
				$('#popup_module_download').find('.popup_cancel_button').click(function()
				{
					$('#popup_module_download').hide();
				});
				
				$('#popup_module_download').find('.popup_delete_button').click(function()
				{
					var parent = active_module.parent();
					modules.splice(active_module.attr('module_id'),1);
					active_module.remove();
					resize_col(parent);
					$('#popup_module_download').hide();
				});
				
				
				$('#file_upload_button').click(function()
				{
					$('#file_upload_input').click();
				});
				
						
			});
			
			$('#file_upload_input').change(function()
			{
				var uploadpath = $(this).attr('uploadpath');
				var xhr = new XMLHttpRequest();		
				var fd = new FormData;
				fd.append('data', this.files[0]);
				fd.append('filename', this.files[0].name);
				fd.append('uploadpath', uploadpath);
				
				xhr.addEventListener('load', function(e) 
				{
					var ret = $.parseJSON(this.responseText);
					if(ret.success)
					{
						modules[active_module.attr('module_id')].setFilename(ret.filename);
					}
					else
					{
						alert('Error while uploading');
					}
			    });
				
				xhr.open('post', rootUrl + 'entities/Item/uploadFile');
				xhr.send(fd);
			});
		},
		
		unbindListeners: function()
		{

		},
		
		getSaveData: function()
		{
			var ret = 
			{
				column: this.parent.attr('id'),
				top: parseInt(this.module.position().top),
				fname: this.fname,
				description: this.description,
				type: 'download',
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.content);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setFontColor(properties.font_color);
			this.setFontSize(properties.font_size);
			this.setTextAlign(properties.align);
			this.setSidebarImage(properties.right_side_img);
			this.setSidebarText(properties.right_side_img_text);
		},		
		
		setFilename: function(fname)
		{
			this.fname = fname;
			//$('.prop_download_fname').val(fname);
		},
		
		setDescription: function(desc)
		{
			this.description = desc;
		},
		
	}
	
	return new_elem;
}