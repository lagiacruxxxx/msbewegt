var id_increment = 0;
var modules = [];
var active_module = null;
var CKInternalUrls = null;

$(document).ready(function()
{	
	toggleScrollListeners(true);
	toggleEditItemListeners(true);
	toggleButtonListeners(true);
	igniteDragging();
	igniteCKEditor();
	
	initModules();
	//fitPreviewImages();
});


function initModules()
{
	$('.module_backend').each(function()
	{
		if($(this).hasClass('module_text'))
		{
			modules[$(this).attr('module_id')] = new_module_text($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].text = $(this).html();
		}
		
		if($(this).hasClass('module_image'))
		{
			modules[$(this).attr('module_id')] = new_module_image($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].filename = $(this).find('img').attr('fname');
		}
		
		if($(this).hasClass('module_bulletpoint'))
		{
			modules[$(this).attr('module_id')] = new_module_bulletpoint($(this).attr('module_id'), $(this).parent());
		}
		
		if($(this).hasClass('module_video'))
		{
			modules[$(this).attr('module_id')] = new_module_video($(this).attr('module_id'), $(this).parent());
		}
		
		if($(this).hasClass('module_html'))
		{
			modules[$(this).attr('module_id')] = new_module_html($(this).attr('module_id'), $(this).parent());
			modules[$(this).attr('module_id')].html_code = $('#module_html_container_' + $(this).attr('module_html_id')).val();
		}
		
		if($(this).hasClass('module_headline'))
		{
			modules[$(this).attr('module_id')] = new_module_headline($(this).attr('module_id'), $(this).parent());
		}
		
		if($(this).hasClass('module_download'))
		{
			modules[$(this).attr('module_id')] = new_module_download($(this).attr('module_id'), $(this).parent());
		}
		
		if(id_increment <= parseInt($(this).attr('module_id')) +1)
			id_increment = parseInt($(this).attr('module_id')) +1;
		
		modules[$(this).attr('module_id')].init();
	});

	resize_col($('#itemcol_big'));
	resize_col($('#itemcol_small'));
}

function toggleButtonListeners(toggle)
{
	if(toggle)
	{
		$('.item_save').click(function()
		{
			saveItem();
		});
		
		$('.item_cancel').click(function()
		{
			switch(itemType)
			{
				case "0":
					window.location.href = rootUrl + 'entities/Item/items';
					break;
					
				case "1":
					window.location.href = rootUrl + 'entities/Blog/entries';
					break;
				
				case "2":
					window.location.href = rootUrl + 'entities/News/articles';
					break;
				
			}
		});
		
		$('.item_detail_type_switch').on('click', function()
		{
			$('.item_detail_type_switch').toggleClass('item_detail_type_active');
			toggleDetailType($('.item_detail_type_switch.item_detail_type_active').attr('detail_type'));
		});
	}
	else
	{
		$('.item_save').unbind('click');
		$('.item_cancel').unbind('click');
	}
}



function toggleDetailType(detail_type)
{
	if(detail_type == 0)
	{
		$('#item_detail_html').hide();
		$('#item_detailimg').show();
		$('#item_detailimg_credits').show();
	}
	else
	{
		$('#item_detailimg').hide();
		$('#item_detailimg_credits').hide();
		$('#item_detail_html').show();
	}
}


function saveItem()
{
	
	var mods = [];
	for(var i = 0 ; i < id_increment ; i++)
	{
		if(modules[i] !== undefined)
		{
			mods.push(modules[i].getSaveData());
		}
	}
	
	switch(itemType)
	{
		case "0":
			var save_url = rootUrl + 'entities/Item/save_item' 
			break;
			
		case "1":
			var save_url = rootUrl + 'entities/Blog/save_blog_entry' 
			break;			
			
		case "2":
			var save_url = rootUrl + 'entities/News/save_news' 
			break;	
			
		case "3":
			var save_url = rootUrl + 'entities/Partner/save_partner' 
			break;
	}
	
	$.ajax(
	{
		url: save_url,
		data: {
			id: $('#item_container').attr('item_id'),
			
			detail_img: $('#item_detailimg img').attr('filename'),
			detail_html: $('#item_detail_html_content').html(),
			detail_type: $('.item_detail_type_switch.item_detail_type_active').attr('detail_type'),
			detail_html_mobile: $('#item_detail_html_content_mobile').html(),
			header: $('#item_header').html(),
			modules: mods,
		},
		method: 'POST',
		success: function(data)
		{
			var ret = $.parseJSON(data);
			
			if(ret.success)
			{
				alert('Save successful!');
			}
			else
			{
				alert('Error while saving');
			}
		}
	});	
}


function igniteCKEditor()
{
	
	
	$('#module_text_editor').ckeditor();	
	$('#module_bulletpoint_editor').ckeditor();
}


function igniteDragging()
{
	$('#module_container li').draggable(
	{
		helper: 'clone'
	});
	
	
	$( ".col" ).droppable(
	{
		accept: '#module_container li',
		drop: function( event, ui ) 
		{
			new_module(ui.draggable.attr('type'), $(this));
	    }
    });
	
	
	$('.module_backend').draggable(
	{
		cursor: 'mode',
		grid: [10,10],
		drag: function(event, ui)
		{
			ui.position.left = 0;
			ui.position.top = Math.max(0, ui.position.top);
			if(ui.position.top + ui.helper.height() > ui.helper.parent().height())
				ui.helper.parent().height(ui.position.top + ui.helper.height());
		},
	});
}


function new_module(module_type, parent)
{
	var new_module = null;
	
	switch(module_type)
	{
		case 'text':
			modules[id_increment] = new_module_text(id_increment, parent);
			break;
		case 'image':
			modules[id_increment] = new_module_image(id_increment, parent);
			setTimeout(function()
			{
				resize_col(parent);
			}, 200);
			break;
		case 'bulletpoint':
			modules[id_increment] = new_module_bulletpoint(id_increment, parent);
			break;
		case 'video':
			modules[id_increment] = new_module_video(id_increment, parent);
			setTimeout(function()
			{
				resize_col(parent);
			}, 200);
			break;
		case 'html':
			modules[id_increment] = new_module_html(id_increment, parent);
			modules[id_increment].html_code = '';
			break;	
		case 'headline':
			modules[id_increment] = new_module_headline(id_increment, parent);
			break;
		case 'download':
			modules[id_increment] = new_module_download(id_increment, parent);
			break;
	}
	
	parent.append(modules[id_increment].getPrototypeHTML());
	modules[id_increment].init();
	resize_col(parent);
	
	id_increment++;
}


function toggleScrollListeners(toggle)
{
	if(toggle)
	{
		$('#content').scroll(function() 
		{
			$('#control_container').css({'top': $('#content').scrollTop() + 100});
		});
	}
	else
	{
		$('#content').unbind('scroll');
	}
}

function nl2br(str, is_xhtml) 
{   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function br2nl(str)
{
    return str.replace(/<br>/g, "\r");
};


function resize_col($col)
{
	var min_height = 0;
	$col.find('.module_backend').each(function()
	{
		if(min_height < parseInt($(this).height()) + $(this).position().top + 2)
			min_height = parseInt($(this).height()) + $(this).position().top + 2
	});
	
	/*if(min_height == 0)
		min_height = 40;*/
	
	$col.css({'min-height': min_height == 0 ? 40 : min_height});
	return min_height;
}


function toggleEditItemListeners(toggle)
{
	if(toggle)
	{
		
		/********************************************************************************************
		 * HEADLINE
		 *********************************************************************************************/
		$('#item_headline').dblclick(function()
		{
			popup_edit_text($(this));
		});
		
		/********************************************************************************************
		 * DETAIL HTML
		 *********************************************************************************************/
		$('#item_detail_html').dblclick(function()
		{
			popup_edit_html($('#item_detail_html_content'));
		});
		
		/********************************************************************************************
		 * DETAIL PHOTOCREDITS
		 *********************************************************************************************/
		$('#item_detailimg_credits').dblclick(function()
		{
			popup_edit_text($(this));
		});
		
		/********************************************************************************************
		 * HEADER
		 *********************************************************************************************/
		$('#item_header').dblclick(function()
		{
			popup_edit_header($(this));
		});
		
		
		/********************************************************************************************
		 * DETAILIMAGE
		 *********************************************************************************************/
		$('#item_detailimg img').dblclick(function()
		{
			if($(this).attr('filename') == '' || $(this).attr('filename') == 'image_upload_placeholder.png' )
				$(this).parent().find('input').click();
			else
			{
				$('#detail_img_dialog').dialog(
				{
					modal: true,
					buttons: [
	          			{
	          				text: 'Delete',
	          				click: function()
	          				{
	          					$('#item_detailimg img').attr('src', rootUrl + 'items/uploads/detailimg/image_upload_placeholder.png');
	          					$('#item_detailimg img').attr('filename', 'image_upload_placeholder.png');
	          					$( this ).dialog( "close" );
	          				}
	          			},
	          			{
	          				text: 'Upload new',
	          				click: function()
	          				{
	          					$( this ).dialog( "close" );
	          					$('#item_detailimg img').parent().find('input').click();
	          				}
	          			}
					],
				});
			}
		});
		
		$('#item_detailimg input').change(function()
		{
			var uploadpath = $(this).attr('uploadpath');
			var xhr = new XMLHttpRequest();		
			var fd = new FormData;
			var files = this.files;
			
			fd.append('data', files[0]);
			fd.append('filename', files[0].name);
			fd.append('uploadpath', uploadpath);
			
			xhr.addEventListener('load', function(e) 
			{
				var ret = $.parseJSON(this.responseText);
				
				if(ret.success)
				{
					$('#item_detailimg img').attr('src', rootUrl + $('#item_detailimg input').attr('uploadpath') + '/' + ret.filename);
					$('#item_detailimg img').attr('filename', ret.filename);
				}
				else
				{
					alert('Error while uploading');
				}
		    });
			
			xhr.open('post', rootUrl + 'entities/Item/upload_image');
			xhr.send(fd);
		});

		
		
	}
	else
	{
		$('#item_header').unbind('dblclick');
		$('#item_detailimg img').unbind('dblclick');
		$('#item_headline').unbind('dblclick');
		$('#item_rel_tags').unbind('dblclick');
	}
}


function popup_edit_html(text_element)
{
	$('#popup_edit_html').find('.popup_cancel_button').unbind('click');
	$('#popup_edit_html').find('.popup_save_button').unbind('click');

	$('#popup_edit_html').find('.popup_edit_container.desktop textarea').val($('#item_detail_html_content').html());
	$('#popup_edit_html').find('.popup_edit_container.mobile textarea').val($('#item_detail_html_content_mobile').html());
	$('#popup_edit_html').show();
	
	$('#popup_edit_html').find('.popup_save_button').click(function()
	{
		$('#item_detail_html_content').html($('#popup_edit_html').find('.popup_edit_container.desktop textarea').val());
		$('#item_detail_html_content_mobile').html($('#popup_edit_html').find('.popup_edit_container.mobile textarea').val());
		$('#popup_edit_html').hide();
	});
	
	$('#popup_edit_html').find('.popup_cancel_button').click(function()
	{
		$('#popup_edit_html').hide();
	});
}


function popup_edit_text(text_element)
{
	$('#popup_edit_text').find('.popup_cancel_button').unbind('click');
	$('#popup_edit_text').find('.popup_save_button').unbind('click');

	$('#popup_edit_text').find('textarea').val(text_element.text());
	$('#popup_edit_text').show();
	
	$('#popup_edit_text').find('.popup_save_button').click(function()
	{
		text_element.html(nl2br($('#popup_edit_text').find('textarea').val(), false));
		$('#popup_edit_text').hide();
	});
	
	$('#popup_edit_text').find('.popup_cancel_button').click(function()
	{
		$('#popup_edit_text').hide();
	});
}

function popup_edit_header(element)
{
	var html = "";
	
	$('#popup_edit_text').find('textarea').val(element.html());
	
	$('#popup_edit_text').find('.popup_cancel_button').unbind('click');
	$('#popup_edit_text').find('.popup_save_button').unbind('click');
	
	$('#popup_edit_text').show();
	
	$('#popup_edit_text').find('.popup_save_button').click(function()
	{
		if($('#popup_edit_text').find('textarea').val() != '')
			var html = $('#popup_edit_text').find('textarea').val();
		else
			var html = '';
		
		$('#popup_edit_text').hide();
		element.html(html);
	});
	
	$('#popup_edit_text').find('.popup_cancel_button').click(function()
	{
		$('#popup_edit_text').hide();
	});	
}

/*********************************************************************************************************************************************************************
 * RELATED TAGS
 **********************************************************************************************************************************************************************/
function popup_edit_rel_tags()
{
	toggleRelatedMetatagsListeners(false);
	toggleRelatedMetatagSubListeners(false);
	
	$('#related_tags_selected').empty();
	$('#related_tags_metatags span').show();
	$('#item_rel_tags span').each(function()
	{
		$('#related_tags_selected').append('<span metatag_id="' + $(this).attr('metatag_id') + '" url="' + $(this).attr('url') + '">' + $(this).text() + '</span>');
		$('.related_tags_metatags span[metatag_id="' + $(this).attr('metatag_id') + '"]').hide();
	});
	
	toggleRelatedMetatagsListeners(true);
	toggleRelatedMetatagSubListeners(true);
	
	$('#popup_related_tags').show();
	
	
}

function toggleRelatedMetatagsListeners(toggle)
{
	if(toggle)
	{
		$('#external_tag_add').on('click', function()
		{
			$('#related_tags_selected').append('<span metatag_id="null" url="' + $('#external_tag_url').val() + '">' + $('#external_tag_text').val() + '</span>');
			toggleRelatedMetatagSubListeners(false);
			toggleRelatedMetatagSubListeners(true);
		});
		
		$('#related_tags_category').change(function()
		{
			$(this).parent().find('.related_tags_metatags').hide();
			$(this).parent().find('.related_tags_metatags[category_id="' + $(this).val() + '"]').show();
		});
		
		$('.related_tags_metatags span').on('click', function()
		{
			//$(this).clone(false).appendTo($('#related_tags_selected'));
			$('#related_tags_selected').append('<span metatag_id="' + $(this).attr('metatag_id') + '" url="null">' + $(this).text() + '</span>');
			$(this).hide();
			toggleRelatedMetatagSubListeners(false);
			toggleRelatedMetatagSubListeners(true);
		});

		$('#popup_related_tags').find('.popup_save_button').click(function()
		{
			$('#item_rel_tags').empty();
			$('#related_tags_selected span').each(function()
			{
				$('#item_rel_tags').append('<span metatag_id="' + $(this).attr('metatag_id') + '" url="' + $(this).attr('url') + '">' + $(this).text() + '</span>');
			});
			$('#popup_related_tags').hide();
		});
		
		$('#popup_related_tags').find('.popup_cancel_button').click(function()
		{
			$('#popup_related_tags').hide();
		});	
	}
	else
	{
		$('#external_tag_add').off('click');
		$('#related_tags_category').unbind('change');
		$('.related_tags_metatags span').off('click');
		$('#popup_related_tags').find('.popup_cancel_button').unbind('click');
		$('#popup_related_tags').find('.popup_save_button').unbind('click');
	}
}

function toggleRelatedMetatagSubListeners(toggle)
{
	if(toggle)
	{
		$('#related_tags_selected span').on('click', function()
		{
			$('.related_tags_metatags span[metatag_id="' + $(this).attr('metatag_id') + '"]').show();
			$(this).remove();
		});		
	}
	else
	{
		$('#related_tags_selected span').off('click');
	}
}


/*********************************************************************************************************************************************************************
 * RELATED ITEMS
 **********************************************************************************************************************************************************************/
function popup_edit_rel_items()
{
	toggleRelatedItemsListener(false);
	
	$('#related_items_selected').empty();
	$('.item_rel_item').each(function()
	{
		$('#related_items_selected').append('<span item_id="' + $(this).attr('item_id') + '" filename="' + $(this).find('img').attr('filename') + '">' + $(this).find('.item_rel_item_name').text() + '</span>')
	});
	
	toggleRelatedItemsListener(true);
	$('#popup_related_items').show();
}

function toggleRelatedItemsListener(toggle)
{
	if(toggle)
	{
		$('#popup_related_items').find('.popup_save_button').click(function()
		{
			$('#item_rel_items').empty();
			$('#related_items_selected span').each(function()
			{
				$('#item_rel_items').append('<div class="item_rel_item" item_id="' + $(this).attr('item_id') + '"><img src="' + rootUrl + 'items/uploads/detailimg/' + $(this).attr('filename') + '" /><div class="item_rel_item_name unselectable">' + $(this).text() + '</div></div>');
			});
			fitPreviewImages();
			$('#popup_related_items').hide();
		});
		
		$('#popup_related_items').find('.popup_cancel_button').click(function()
		{
			$('#popup_related_items').hide();
		});
		
		$('#related_items_category').change(function()
		{
			$('#popup_related_items').find('.related_items_metatags').hide();
			$('#popup_related_items').find('.related_items_metatags[category_id="' + $(this).val() + '"]').show();
		});
		
		$('#popup_related_items span').click(function()
		{
			if($(this).parent().hasClass('related_items_metatags'))
			{
				$(this).clone(true).appendTo($('#related_items_tags_selected'));
				$(this).hide();
			}
			else
			{
				$('.related_items_metatags span[metatag_id="' + $(this).attr('metatag_id') + '"]').show();
				$(this).remove();
			}
			
			var metatags = [];
			$('#related_items_tags_selected span').each(function()
			{
				metatags.push($(this).attr('metatag_id'));
			});
			
			$.ajax(
			{
				url: rootUrl + 'entities/Item/getItemsPerMetatags',
				data: {'metatags': metatags},
				method: 'POST',
				success: function(data)
				{
					var ret = $.parseJSON(data);
					if(ret.success)
					{
						$('#related_items_itemlist').empty();
						for(var i = 0 ; i < ret.items.length ; i++)
						{
							var style = "";
							if($('#related_items_selected span[item_id="' + ret.items[i].id + '"]').length != 0)
								style = 'style="display: none;"';
								
							$('#related_items_itemlist').append('<span ' + style + '  item_id="' + ret.items[i].id + '" filename="' + ret.items[i].detailimg + '">' + ret.items[i].name + '</span>');
						}
						
						$('#related_items_itemlist span, #related_items_selected span').unbind('click');
						$('#related_items_itemlist span, #related_items_selected span').click(function()
						{
							if($(this).parent().attr('id') == 'related_items_itemlist')
							{
								console.log('select');
								$(this).clone(true).appendTo('#related_items_selected');
								$(this).hide();
							}
							else
							{
								$('#related_items_itemlist span[item_id="' + $(this).attr('item_id') + '"]').show();
								$('#related_items_selected span[item_id="' + $(this).attr('item_id') + '"]').remove();
							}
						});
					}
					else
					{
						alert('Error while retrieving items');
					}
				}
			});	
		});		
	}
	else
	{
		$('#popup_related_items span').unbind('click')
		$('#related_items_category').unbind('change');
		$('#popup_related_items').find('.popup_cancel_button').unbind('click');
		$('#popup_related_items').find('.popup_save_button').unbind('click');
	}
}

