


/***********************************
 * DEVICE SPECIFIC
 ************************************/


$(document).ready(function()
{	
	toggleMenuListeners(true);
	ignitePackery();
	igniteBlogListeners();
	igniteSliders();
	setTimeout(function()
	{
		igniteItem();
	}, 200)
	
});




function toggleMenuListeners(toggle)
{
	if(toggle)
	{
		$('.about_submenu').on('click', function()
		{
			$('#menulist').toggleClass('menuhidden');
		});
		
		$('#menusearch_input, #mobile_search').on('keypress', function(event)
		{
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				window.location.href = rootUrl + 'search?input=' + $(this).val();
			}
		});
		
		$('#search_toggle').on('click', function()
		{
			$('#searchbar').toggleClass('menuhidden');
		});
		
		$('.menusize_select').on('click', function()
		{
			docCookies.setItem('msbewegt_size', $(this).attr('id'), Infinity, null, null , false);
			window.location.reload();
		});
	}
	else
	{
		$('.about_submenu').off('click');
		$('#menusearch_input, #mobile_search').off('keyup');
		$('#search_toggle').off('click');
		$('.menusize_select').off('click');
	}
}


function ignitePackery()
{
	$('#mosaic').packery(
	{
		'itemSelector': '.mosaic_item',
		'gutter': 10
	});
	
	$('#blog_entries').isotope(
	{
		'itemSelector': '.blog_entry',
		'layoutMode': 'masonry',
		'masonry':
		{
			'gutter': 10,
			'columnWidth': 280,
		}
	});
}

function igniteBlogListeners()
{
	$('.blogger').on('click', function()
	{
		if($(this).hasClass('inactive'))
		{
			$('.blogger').addClass('inactive');
			$(this).removeClass('inactive');
			
			$('.blogger .blogger_active').hide();
			$('.blogger .blogger_inactive').show();
			$(this).find('.blogger_active').show();
			$(this).find('.blogger_inactive').hide();
	
			$('#blog_entries').isotope(
			{
				filter: '.' + $(this).attr('blogger'),
			});
		}
		else
		{
			if($('.blogger.inactive').length > 0)
			{
				$('.blogger').removeClass('inactive');
				$('.blogger .blogger_active').show();
				$('.blogger .blogger_inactive').hide();	
				
				$('#blog_entries').isotope(
				{
					filter: '',
				});
			}
			else
			{
				$('.blogger').addClass('inactive');
				$(this).removeClass('inactive');
				
				$('.blogger .blogger_active').hide();
				$('.blogger .blogger_inactive').show();
				$(this).find('.blogger_active').show();
				$(this).find('.blogger_inactive').hide();
		
				$('#blog_entries').isotope(
				{
					filter: '.' + $(this).attr('blogger'),
				});				
			}

		}
		
	});
}


function igniteItem()
{
	resize_col($('#itemcol_big'));
	resize_col($('#itemcol_small'));
}

function resize_col($col)
{
	var min_height = 0;
	$col.find('.module').each(function()
	{
		if(min_height < parseInt($(this).height()) + $(this).position().top + 2)
			min_height = parseInt($(this).height()) + $(this).position().top + 2
	});
	
	if(min_height == 0)
		min_height = 40;
	
	$col.css({'min-height': min_height});
	return min_height;
}

function igniteSliders()
{
	setInterval(function()
	{
		var act =  $('.slider_item[status="active"]').attr('i');
		act++;
		if($('.slider_item[i=' + act + ']').length <= 0)
			act = 0;
			
		
		$('.slider_item[status="active"]').animate({'left': '-=1000px'}, 1000, function()
		{
			$(this).attr('status', 'inactive');
			$(this).css('left', 1000);
		});
		$('.slider_item[i=' + act + ']').animate({'left': '-=1000px'}, 1000, function()
		{
			$(this).attr('status', 'active');
		});
	}, 5000);
}
