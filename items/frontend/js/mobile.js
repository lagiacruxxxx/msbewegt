var item_activate_delay = false;

/***********************************
 * DEVICE SPECIFIC
 ************************************/


$(document).ready(function()
{	
	resize_mobile();
	toggleMobileListeners(true);
});

function resize_mobile()
{
	$('#menu_mobile').css({'height': $(window).height(), 'top': $(window).height()});
}

function toggleMobileListeners(toggle)
{
	if(toggle)
	{
		$('#mobile_menu').on('click touchend', function()
		{
			toggleMobileMenu(true);
		});
		
		$('#menu_mobile_close').on('click touchend', function()
		{
			toggleMobileMenu(false);
		});
	}
	else
	{
		$('#mobile_menu').off('click touchend');
		$('#menu_mobile_close').off('click touchend');
	}
}



function toggleMobileMenu(toggle)
{
	toggleMobileListeners(false);
	if(toggle)
	{
		$('#menu_mobile').animate({'top': 0}, 250, function()
		{
			toggleMobileListeners(true);
			$('#menu_mobile').removeClass('hidden');
		});
	}
	else
	{
		$('#menu_mobile').animate({'top': $(window).height()}, 250, function()
		{
			toggleMobileListeners(true);
			$('#menu_mobile').addClass('hidden');
		});
	}
}


/***********************************
 * OVERWRITING FROM DESKTOP.JS
 ************************************/

function resize_col($col)
{
	
	}